```
|-- vue-pantry-dashboard-pro
    |-- App.vue
    |-- main.js
    |-- polyfills.js
    |-- assets
    |   |-- css
    |   |   |-- nucleo
    |   |-- sass
    |       |-- pantry.scss
    |       |-- core
    |       |-- custom
    |-- components
    |   |-- index.js
    |   |-- Breadcrumb
    |   |   |-- Breadcrumb.vue
    |   |   |-- BreadcrumbItem.vue
    |   |   |-- RouteBreadcrumb.vue
    |   |-- Cards
    |   |   |-- Card.vue
    |   |   |-- StatsCard.vue
    |   |-- Charts
    |   |   |-- BarChart.js
    |   |   |-- DoughnutChart.js
    |   |   |-- LineChart.js
    |   |   |-- PieChart.js
    |   |   |-- config.js
    |   |   |-- globalOptionsMixin.js
    |   |   |-- optionHelpers.js
    |   |-- Collapse
    |   |   |-- Collapse.vue
    |   |   |-- CollapseItem.vue
    |   |-- Inputs
    |   |   |-- BaseCheckbox.vue
    |   |   |-- BaseInput.vue
    |   |   |-- BaseRadio.vue
    |   |   |-- DropzoneFileUpload.vue
    |   |   |-- FileInput.vue
    |   |   |-- HtmlEditor.vue
    |   |   |-- IconCheckbox.vue
    |   |   |-- TagsInput.vue
    |   |-- Navbar
    |   |   |-- BaseNav.vue
    |   |   |-- NavbarToggleButton.vue
    |   |-- NotificationPlugin
    |   |   |-- Notification.vue
    |   |   |-- Notifications.vue
    |   |   |-- index.js
    |
    |   |-- Shared
    |       |-- Badge.vue
    |       |-- BaseAlert.vue
    |       |-- BaseButton.vue
    |       |-- BaseDropdown.vue
    |       |-- BaseHeader.vue
    |       |-- BasePagination.vue
    |       |-- BaseProgress.vue
    |       |-- BaseSwitch.vue
    |       |-- BaseTable.vue
    |       |-- CloseButton.vue
    |       |-- LoadingPanel.vue
    |       |-- Modal.vue
    |       |-- NavbarToggleButton.vue
    |       |-- Slider.vue
    |   |-- SidebarPlugin
    |   |   |-- SideBar.vue
    |   |   |-- SidebarItem.vue
    |   |   |-- index.js
    |   |-- Tabs
    |   |   |-- Tab.vue
    |   |   |-- Tabs.vue
    |   |-- Timeline
    |   |   |-- TimeLine.vue
    |   |   |-- TimeLineItem.vue
    |   |-- Widgets
    |   |    |-- CalendarWidget.vue
    |   |    |-- CreditCard.vue
    |   |    |-- MembersCard.vue
    |   |    |-- PaypalCard.vue
    |   |    |-- ProgressTrackList.vue
    |   |    |-- StatsCards.vue
    |   |    |-- TaskList.vue
    |   |    |-- TimelineCard.vue
    |   |    |-- VectorMapCard.vue
    |   |    |-- VisaCard.vue
    |   |-- WorldMap
    |       |-- AsyncWorldMap.vue
    |       |-- WorldMap.vue
    |-- directives
    |   |-- click-ouside.js
    |-- plugins
    |   |-- dashboard-plugin.js
    |   |-- globalComponents.js
    |   |-- globalDirectives.js
    |-- routes
    |   |-- router.js
    |   |-- routes.js
    |   |-- starterRouter.js
    |-- util
    |   |-- throttle.js
    |-- views
     


```
