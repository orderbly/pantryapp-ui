
import { eraseCookie } from '../constants/cookies'

export async function apiHandler(response) {
  if (response.message && response.message.includes("401")) {
    eraseCookie('token')
    eraseCookie('company')
    location.replace('/')
  } else {
    return true
  }
}