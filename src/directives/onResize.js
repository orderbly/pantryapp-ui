export default {
  onResize() {
    if (window.innerWidth <= 768) {
      this.isMobile = true
      return window.innerWidth
    } else {
      this.isMobile = false
      return window.innerWidth
    }
  }
}
