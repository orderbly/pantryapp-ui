import BaseInput from '@/components/Inputs/BaseInput.vue'
import BaseDropdown from '@/components/Shared/BaseDropdown.vue'
import Card from '@/components/Cards/Card.vue'
import Modal from '@/components/Shared/Modal.vue'
import BottomModal from '@/components/Modals/BottomModal.vue'
import StatsCard from '@/components/Cards/StatsCard.vue'
import BaseButton from '@/components/Shared/BaseButton.vue'
import Badge from '@/components/Shared/Badge.vue'
import RouteBreadcrumb from '@/components/Breadcrumb/RouteBreadcrumb'
import BaseCheckbox from '@/components/Inputs/BaseCheckbox.vue'
import GlobalAddressSearch from '@/components/Inputs/GlobalAddressSearch.vue'
import BaseSwitch from '@/components/Shared/BaseSwitch.vue'
import BaseRadio from '@/components/Inputs/BaseRadio'
import BaseProgress from '@/components/Shared/BaseProgress'
import BaseTable from '@/components/Shared/BaseTable'
import BaseUpload from '@/components/Shared/BaseUpload'
import BaseLoadingTable from '@/components/Shared/BaseLoadingTable'
import BasePagination from '@/components/Shared/BasePagination'
import BaseAlert from '@/components/Shared/BaseAlert'
import BaseNav from '@/components/Navbar/BaseNav'
import BaseHeader from '@/components/Shared/BaseHeader'
import { Input, Tooltip, Popover } from 'element-ui'
/**
 * You can register global components here and use them as a plugin in your main Vue instance
 */

const GlobalComponents = {
  install(Vue) {
    Vue.component(Badge.name, Badge)
    Vue.component(GlobalAddressSearch.name, GlobalAddressSearch)
    Vue.component(BaseAlert.name, BaseAlert)
    Vue.component(BaseButton.name, BaseButton)
    Vue.component(BaseCheckbox.name, BaseCheckbox)
    Vue.component(BaseHeader.name, BaseHeader)
    Vue.component(BaseUpload.name, BaseUpload)
    Vue.component(BaseLoadingTable.name, BaseLoadingTable)
    Vue.component(BaseTable.name, BaseTable)
    Vue.component(BaseInput.name, BaseInput)
    Vue.component(BaseDropdown.name, BaseDropdown)
    Vue.component(BaseNav.name, BaseNav)
    Vue.component(BasePagination.name, BasePagination)
    Vue.component(BaseProgress.name, BaseProgress)
    Vue.component(BaseRadio.name, BaseRadio)
    Vue.component(BaseSwitch.name, BaseSwitch)
    Vue.component(Card.name, Card)
    Vue.component(Modal.name, Modal)
    Vue.component(BottomModal.name, BottomModal)
    Vue.component(StatsCard.name, StatsCard)
    Vue.component(RouteBreadcrumb.name, RouteBreadcrumb)
    Vue.component(Input.name, Input)
    Vue.component(BaseTable.name, BaseTable)
    Vue.use(Tooltip)
    Vue.use(Popover)
  }
}

export default GlobalComponents
