import clickOutside from '@/directives/click-ouside.js';
import deliveryStatusColour from '@/directives/deliveryStatus.js';

/**
 * You can register global directives here and use them as a plugin in your main Vue instance
 */

const GlobalDirectives = {
  install(Vue) {
    Vue.directive('click-outside', clickOutside);

    Vue.directive('delivery-status-colour', {
      bind(el) {
        let value = el.innerText
        if (value == "In preperation") {
          el.style.color = "#5e62e4"
        } else if (value == "Delivery pending") {
          el.style.color = "#fb6340"
        } else if (value == "Delivery in progress") {
          el.style.color = "#11cdef"
        } else if (value == "Delivered") {
          el.style.color = "#00c781"
        }
      }
    })

    Vue.directive('order-status-colour', {
      bind(el) {
        let value = el.innerText
        if (value.includes("Paid")) {
          el.style.color = "#5e62e4"
        } else if (value.includes("Accepted")) {
          el.style.color = "#00c781"
        } else if (value.includes("Pending")) {
          el.style.color = "#5603ad"
        } else if (value.includes("Declined")) {
          el.style.color = "#f5365c"
        } else if (value.includes("Cancelled")) {
          el.style.color = "#8898aa"
        } else {
          el.style.color = 'grey'
        }
      }
    })
  }
}


export default GlobalDirectives;
