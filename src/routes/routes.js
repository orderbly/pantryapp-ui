import Vue from 'vue'
import Router from 'vue-router'
import DashboardLayout from '../views/Layout/DashboardLayout.vue'
import PageNotFound from '../views/GeneralViews/NotFoundPage.vue'
import AuthLayout from '../views/Register/layout/AuthLayout.vue'
Vue.use(Router)

export default new Router({
  mode: 'history',

  routes: [
    { 
      path: '*', 
      component: PageNotFound
    },
    {
      path: '/select_company',
      name: 'select_company',
      component: () =>
        import(
          '../views/Register/views/SelectCompany.vue'
        )
    },
    {
      path: '/password_resets/:token_id',
      name: 'ResetPassword',
      props: true,
      component: () =>
        import(
          '../views/Register/views/ResetPassword.vue'
        )
    },
    {
      path: '/register',
      name: 'register',
      component: () =>
        import(
          '../views/Register/views/Register.vue'
        )
    },
    {
      path: '/supplier_register',
      name: 'supplier_register',
      component: () =>
        import(
          '../views/Register/views/SupplierRegister.vue'
        )
    },
    {
      path: '/login',
      name: 'login',
      component: () =>
        import(
          '../views/Register/views/Login.vue'
        )
    },
    {
      path: '/existing_login/:encodedCredentials',
      name: 'existing_login',
      props: route => ({ credentials: route.query.q }),
      component: () =>
        import(
          '../views/Register/views/Login.vue'
        )
    },
    {
      path: '/restaurant_register',
      name: 'restaurant_register',
      component: () =>
        import(
          '../views/Register/views/RestaurantRegister.vue'
        )
    },
    {
      path: '/',
      redirect: 'suppliers',
      component: DashboardLayout,
      children: [
        {
          path: '/dashboard',
          name: 'dashboard',
          // route level code-splitting
          // this generates a separate chunk (about.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          component: () =>
            import(
              '@/views/Dashboard/Dashboard.vue'
            )
        },
        {
          path: '/suppliers',
          name: 'suppliers',
          component: () =>
            import(
              '@/views/Suppliers/views/Suppliers.vue'
            )
        },
        {
          path: '/my_deliveries',
          name: 'myDeliveries',
          component: () =>
            import(
              '@/fulfilment_views/Deliveries/views/MyDeliveries.vue'
            )
        },
        {
          path: "/receive_order/:id",
          name: "receiveOrder",
          props: true,
          component: () => import("@/fulfilment_views/Deliveries/views/ReceiveOrder")
        },
        {
          path: '/tax_settings',
          name: 'taxSettings',
          component: () =>
            import(
              '@/views/Tax/views/TaxSettings.vue'
            )
        },
        {
          path: '/delivery_settings',
          name: 'deliverySettings',
          component: () =>
            import(
              '@/views/Settings/views/DeliverySettings.vue'
            )
        },
        {
          path: '/calendar',
          name: 'calendar',
          component: () =>
            import(
              '../views/Calendar/Calendar.vue'
            )
        },
        {
          path: '/suppliers/:title',
          name: 'suppliersDetail',
          props: true,
          component: () =>
            import(
              '@/views/Suppliers/views/SupplierDetail.vue'
            )
        },
        {
          path: '/Products',
          name: 'ProductIndex',
          props: true,
          component: () =>
            import(
              '@/views/Products/views/ProductIndex.vue'
            )
        },
        {
          path: '/new_product',
          name: 'NewProduct',
          props: true,
          component: () =>
            import(
              '@/views/Products/views/NewProduct.vue'
            )
        },
        {
          path: '/edit_product/:uuid',
          name: 'EditProduct',
          props: true,
          component: () =>
            import(
              '@/views/Products/views/EditProduct.vue'
            )
        },
        {
          path: '/purchase_orders',
          name: 'OrderIndex',
          props: true,
          component: () =>
            import(
              '@/views/Orders/views/OrderIndex.vue'
            )
        },
        {
          path: '/sales_orders',
          name: 'SalesOrders',
          props: true,
          component: () =>
            import(
              '@/views/Orders/views/SalesOrders.vue'

            )
        },
        {
          path: '/checkout',
          name: 'checkout',
          component: () =>
            import(
              '../views/Checkout/views/Checkout.vue'
            )
        },
        {
          path: '/order/:id',
          name: 'orderDetail',
          props: true,
          component: () =>
            import(
              '@/views/Orders/views/OrderDetail.vue'
            )
        },
        {
          path: '/delivery_zone/:zoneUuid',
          name: 'deliveryZone',
          props: true,
          component: () =>
            import(
              '@/views/Settings/views/DeliveryZone.vue'
            )
        },
        {
          path: '/Calendar',
          name: 'Calendar',
          props: true,
          component: () =>
            import(
              '@/views/Calendar/Calendar.vue'
            )
        },
        {
          path: '/profile',
          name: 'profile',
          component: () =>
            import(
              '@/views/User/views/UserProfile.vue'
            )
        },
        {
          path: '/memberships',
          name: 'memberships',
          component: () =>
            import(
              '@/views/Memberships/views/Memberships.vue'
            )
        },
        {
          path: '/invite_user',
          name: 'iniviteUser',
          component: () =>
            import(
              '@/views/Memberships/views/InviteUser.vue'
            )
        },
        {
          path: '/edit_membership/:uuid',
          name: 'edit_membership',
          component: () =>
            import(
              '@/views/Memberships/views/EditUserMembership.vue'
            )
        },
        {
          path: '/integrations',
          name: 'integrations',
          component: () =>
            import(
              '@/views/Integrations/views/Providers.vue'
            )
        },        
        {
          path: '/price_books',
          name: 'priceBooks',
          component: () =>
            import(
              '@/views/PriceBook/views/PriceBookIndex.vue'
            )
        },
        {
          path: '/customer_statement',
          name: 'customerStatement',
          component: () =>
            import(
              '@/views/CustomerStatement/views/CustomerAgeAnalysis.vue'
            )
        },
        {
          path: '/supplier_statement',
          name: 'suppliersStatement',
          component: () =>
            import(
              '@/views/SupplierStatement/views/SupplierAgeAnalysis.vue'
            )
        },
        {
          path: '/supplier_statement/:supplier_id',
          name: 'supplierStatement',
          component: () =>
            import(
              '@/views/SupplierStatement/views/SupplierStatement.vue'
            )
        },
        {
          path: '/trade_statements/:supplier_uuid/:customer_uuid',
          name: 'trade_statement',
          component: () =>
            import(
              '../views/Statements/views/TradeStatement.vue'
            )
        },
        {
          path: '/customer_statement/:customer_id',
          name: 'customer_analysis',
          component: () =>
            import(
              '@/views/CustomerStatement/views/CustomerStatement.vue'
            )
        },
        {
          path: '/price_book_details/:discount_book_id',
          name: 'priceBookDetail',
          component: () =>
            import(
              '@/views/PriceBook/views/PriceBook.vue'
            )
        },
        {
          path: '/upcoming_deliveries',
          name: 'upcomingDeliveries',
          component: () =>
            import(
              '@/fulfilment_views/Deliveries/views/UpcomingDeliveries.vue'
            )
        },
        {
          path: '/completed_deliveries',
          name: 'completedDeliveries',
          component: () =>
            import(
              '@/fulfilment_views/Deliveries/views/CompletedDeliveries.vue'
            )
        },
        {
          path: '/todays_deliveries',
          name: 'todaysDeliveries',
          component: () =>
            import(
              '@/fulfilment_views/Deliveries/views/TodaysDeliveries.vue'
            )
        },
        {
          path: '/credit_limits',
          name: 'CreditLimit',
          component: () =>
            import(
              '@/views/CreditLimit/views/CreditLimitIndex.vue'
            )
        },
        {
          path: '/waybills',
          name: 'waybills',
          component: () =>
            import(
              '@/views/Waybills/views/WaybillIndex.vue'
            )
        },
        {
          path: '/my_customers',
          name: 'MyCustomers',
          component: () =>
            import(
              '@/views/MyCustomers/views/MyCustomers.vue'
            )
        },
        {
          path: '/my_customer/:customerUuid',
          name: 'MyCustomer',
          component: () =>
            import(
              '@/views/MyCustomers/views/MyCustomer.vue'
            )
        }
      ]
    }
  ],
  // scrollBehavior(to, from, savedPosition) {
  //   return {
  //       x: 0,
  //       y: 0,
  //   };
  // },
})
