require('dotenv').config()

export const subHiddenBreakpoint = 1440
export const menuHiddenBreakpoint = 768

export const defaultLocale = 'en'
export const localeOptions = [
  { id: 'en', name: 'English' },
  { id: 'es', name: 'Español' }
]

export const pantryApi = process.env.VUE_APP_PANTRY_API_URL
export const pusherKey = process.env.VUE_APP_PUSHER_KEY
export const notifyUrl = process.env.VUE_APP_NOTIFY_URL
export const ozowPrivateKey = process.env.VUE_APP_OZOW_PRIVATE_KEY
export const ozowApiKey = process.env.VUE_APP_OZOW_API_KEY
export const testingEnv = process.env.VUE_APP_TESTING_ENV
export const sage_api_url = process.env.VUE_APP_SAGE_API_URL
export const sageApiKey = process.env.VUE_APP_SAGE_API_KEY
export const isProduction = process.env.VUE_APP_ENV === 'production'
export const validateSageLoginUrl = `https://resellers.accounting.sageone.co.za/api/2.0.0/Login/Validate`

export const colors = [
  'light.purple',
  'dark.purple',
  'light.blue',
  'dark.blue',
  'light.green',
  'dark.green',
  'light.orange',
  'dark.orange',
  'light.red',
  'dark.red'
]
