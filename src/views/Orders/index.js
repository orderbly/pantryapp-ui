import OrderHeader from './components/headers/OrderHeader.vue'
import PurchaseOrderHeader from './components/headers/PurchaseOrderHeader.vue'
import OrderIndexTable from './components/tables/OrderIndexTable.vue'
import SalesIndexTable from './components/tables/SalesIndexTable.vue'

export { OrderHeader, OrderIndexTable, SalesIndexTable, PurchaseOrderHeader }
