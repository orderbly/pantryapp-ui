import ProductHeader from './components/headers/ProductHeader.vue'
import ProductsIndexTable from './components/tables/ProductsIndexTable.vue'
import SelectProductModal from './components/modals/SelectProductModal.vue'
import TradegeckoProducts from './components/modals/TradegeckoProducts.vue'

export {
  ProductHeader,
  ProductsIndexTable,
  SelectProductModal,
  TradegeckoProducts
}
