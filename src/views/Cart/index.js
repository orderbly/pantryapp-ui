import CartTable from './components/table/CartTable.vue'
import MobileCartTable from './components/table/MobileCartTable.vue'

export { CartTable, MobileCartTable }
