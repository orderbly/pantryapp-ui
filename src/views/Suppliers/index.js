import SupplierHeader from './components/headers/SupplierHeader.vue'
import SupplierDetailHeader from './components/headers/SupplierDetailHeader.vue'
import SupplierIndexTable from './components/tables/SupplierIndexTable.vue'
import SupplierIndexGrid from './components/grids/SupplierIndexGrid.vue'
import ProductGrid from './components/grids/ProductGrid.vue'
import ProductMobileTable from './components/tables/ProductMobileTable.vue'
import SupplierDetailsModal from './components/modals/SupplierDetailsModal.vue'

export {
  SupplierHeader,
  SupplierDetailHeader,
  SupplierIndexTable,
  SupplierIndexGrid,
  ProductGrid,
  ProductMobileTable,
  SupplierDetailsModal
}
