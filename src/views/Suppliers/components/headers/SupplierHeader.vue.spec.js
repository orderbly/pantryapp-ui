import { shallowMount } from '@vue/test-utils'
import SupplierHeader from '@/components/SupplierHeader.vue'

describe('SupplierHeader', () => {
  const msg = 'new message'
  const company = {
    image: 'test',
    title: 'test',
    description: 'test',
    contact: 'test'
  }
  const wrapper = shallowMount(SupplierHeader, {
    propsData: { company }
  })

  test('should create', () => {
    expect(wrapper).toBeDefined()
  })
})
