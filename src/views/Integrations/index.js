import ProviderIndexTable from './components/tables/ProviderIndexTable.vue'
import InventoryProviderIndexTable from './components/tables/InventoryProviderIndexTable.vue'
import XeroConfigIndexTable from './components/tables/XeroConfigIndexTable.vue'
import XeroContactTable from './components/tables/XeroContactTable.vue'

export {
  InventoryProviderIndexTable,
  ProviderIndexTable,
  XeroConfigIndexTable,
  XeroContactTable
}
