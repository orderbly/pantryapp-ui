
import axios from 'axios'
import { pantryApi } from '@/constants/config'
import {eraseCookie, getCookie} from '../constants/cookies'

const API_URL = pantryApi

const securedAxiosInstance = axios.create({
  baseURL: API_URL,
  withCredentials: false,
  headers: {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${getCookie('access')}`
  }
})

const plainAxiosInstance = axios.create({
  baseURL: API_URL,
  withCredentials: false,
  headers: {
    'Content-Type': 'application/json'
  }
})

securedAxiosInstance.interceptors.response.use(null, error => {
  if (error.response && error.response.config && error.response.status === 401) {
      eraseCookie('access')
      eraseCookie('refresh')
      eraseCookie('csrf')
      eraseCookie('signedIn')
      eraseCookie('company')
        // redirect to signin in case refresh request fails
      location.replace('/')
      return Promise.reject(error)
      // })
  } else {
    return Promise.reject(error)
  }
})

export { securedAxiosInstance, plainAxiosInstance }