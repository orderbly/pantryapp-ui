import Vue from 'vue'
import Vuex from 'vuex'
import companies from './modules/companies'
import priceBooks from './modules/priceBook'
import cart from './modules/cart'
import modals from './modules/modals'
import orders from './modules/orders'
import accountSales from './modules/accountSales'
import creditLimits from './modules/creditLimits'
import deliveries from './modules/deliveries'
import products from './modules/products'
import statements from './modules/statements'
import emails from './modules/emails'
import tax from './modules/tax'
import tags from './modules/tags'
import user from './modules/user'
import integrations from './modules/integrations'
import payGate from './modules/external/payGate'
import oauth from './modules/external/oauth'
import xero from './modules/external/xero'
import sage from './modules/external/sage'
import optimo from './modules/external/optimo'
import tradeGecko from './modules/external/tradeGecko'
import mixpanel from './modules/external/mixpanel'
import fulfilmentOrders from './fulfilment/orderFulfilment'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  modules: {
    cart,
    emails,
    accountSales,
    creditLimits,
    fulfilmentOrders,
    modals,
    orders,
    mixpanel,
    priceBooks,
    products,
    statements,
    tax,
    tags,
    user,
    companies,
    integrations,
    payGate,
    deliveries,
    oauth,
    xero,
    sage,
    tradeGecko,
    optimo
  }
})
