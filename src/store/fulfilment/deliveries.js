import axios from 'axios'
import { securedAxiosInstance, plainAxiosInstance } from '../../axios/index' 
import { pantryApi } from '@/constants/config'
import { apiHandler } from '../../util/apiHandler';
import { getCookie, setCookie } from '../../constants/cookies'
import { FulfillingSquareSpinner } from 'epic-spinners';

let plainApi = plainAxiosInstance
let securedApi = securedAxiosInstance
const isLoggedin = function () {
  return getCookie('access') !== undefined ? true : false
}

const state = {
  myDeliveryOrders: [],
  orderWaybills: [],
  currentReceiveOrder: [],
}

const getters = {
  myDeliveryOrders: state => state.myDeliveryOrders,
  getCurrentReceiveOrder: state => state.currentReceiveOrder,
  orderWaybills: state => state.orderWaybills

}

const mutations = {
  setOrderWaybills(state, data) {
    state.orderWaybills = data;
  },
  setMyDeliveryOrders(state, data) {
    state.myDeliveryOrders = data;
  },
  setCurrentReceiveOrder(state, order) {
    order.order_items.forEach(item => { item.received = null });
    state.currentReceiveOrder = order;
  }

}

const actions = {
  fetchOrderWaybills({ commit }) {
    return securedApi.get(`${pantryApi}/order_waybills`, {})
    .then(res => {
      commit("setOrderWaybills", res.data);
      return true
    })
    .catch(error => {
      return false
    });
  },

  fetchUserDeliveryOrders({ commit }) {
    return securedApi.get(`${pantryApi}/user_deliverable_orders`, {})
    .then(res => {
      if (res) {
        // commit("setMyDeliveryOrders", res.data);
        return true
      }
    })
    .catch((err) => {
      return false
    })
  },

  fetchUserDailyDeliveryOrders({ commit }, {userId, companyId}) {
    return securedApi.get(`${pantryApi}/user_daily_deliverable_orders/${userId}/${companyId}`, {})
    .then(res => {
      if (res) {
        commit("setMyDeliveryOrders", res.data);
        return true
      }
    })
    .catch((err) => {
      return false
    })
  }

}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
