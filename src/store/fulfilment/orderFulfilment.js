import { securedAxiosInstance } from '../../axios/index' 
import { pantryApi } from '@/constants/config'

let securedApi = securedAxiosInstance

const state = {
  myDeliveryOrders: [],
  customClients: [],
  orderWaybills: [],
  currentReceiveOrder: [],
  upcomingOrders: [],
  completedDeliveries: [],
  todaysDeliveries: []
}

const getters = {
  myDeliveryOrders: state => state.myDeliveryOrders,
  getCurrentReceiveOrder: state => state.currentReceiveOrder,
  orderWaybills: state => state.orderWaybills,
  customClients: state => state.customClients,
  upcomingOrders: state => state.upcomingOrders,
  completedDeliveries: state => state.completedDeliveries,
  todaysDeliveries: state => state.todaysDeliveries
}

const mutations = {
  setCustomClients(state, data) {
    state.customClients = data;
  },

  updateActiveDelivery(state, {page, id}) {
    if (page == 'todaysDeliveries') {
      state.todaysDeliveries.orders = state.todaysDeliveries.orders.map((delivery) => {
        delivery.id == id ? delivery.isActive = true : delivery.isActive = false
        return delivery
      })
    }
  },

  setTodaysDeliveries(state, data) {
    state.todaysDeliveries = data;
  },

  allCompletesDeliveries(state, data) {
    state.completedDeliveries = data;
  },

  allDeliveries(state, data) {
    state.customClients = data;
  },

  allUpcomingOrders(state, data) {
    state.upcomingOrders = data
    state.upcomingOrders.orders = state.upcomingOrders.orders.sort((a,b) => (new Date(a.order_date) > new Date(b.order_date)) ? 1 : ((new Date(b.order_date) > new Date(a.order_date)) ? -1 : 0))
    state.upcomingOrders.order_dates = state.upcomingOrders.order_dates.sort((a,b) => (new Date(a) > new Date(b)) ? 1 : ((new Date(b) > new Date(a)) ? -1 : 0))
  },

  setOrderWaybills(state, data) {
    state.orderWaybills = data;
  },

  setMyDeliveryOrders(state, data) {
    state.myDeliveryOrders = data;
  },

  setCurrentReceiveOrder(state, order) {
    order.order_items.forEach(item => { item.received = null });
    state.currentReceiveOrder = order;
  },

  updateOrderStatus(state, order) {
    let itemIndex = state.myDeliveryOrders.findIndex(o => o.id == order.id )
    let existingOrder =  state.myDeliveryOrders[itemIndex]
    state.myDeliveryOrders = state.myDeliveryOrders.filter(o => o.id !== order.id )
    existingOrder.delivery_status = order.delivery_status

    state.myDeliveryOrders.splice(itemIndex, 0, existingOrder)

  }

}

const actions = {

  fetchAllTodaysOrders ({ commit }, id) {
    return securedApi.get(`${pantryApi}/fetch_all_todays_deliveries/${id}`)
    .then(res => {
      commit("setTodaysDeliveries", res.data)
      return true
    })
    .catch(error => {
      return false
    });
  },

  fetchAllDeliveredOrders ({ commit }, {supplier_id, date}) {
    return securedApi.get(`${pantryApi}/fetch_all_completed_deliveries/${supplier_id}/${date}`)
    .then(res => {
      commit("allCompletesDeliveries", res.data)
      return true
    })
    .catch(error => {
      return false
    });
  },

  fetchAllUpcomingOrders ({ commit }, id) {
    return securedApi.get(`${pantryApi}/fetch_all_upcoming_orders/${id}`)
    .then(res => {
      commit("allUpcomingOrders", res.data)
      return true
    })
    .catch(error => {
      return false
    });
  },

  updateDeliveryDetails ({ commit }, order) {
    return securedApi.post(`${pantryApi}/update_delivery_driver_and_date`, order)
    .then(res => {
      if (order.route == '/upcoming_deliveries') {
        commit("allUpcomingOrders", res.data)
      } else if (order.route == '/completed_deliveries') {
        commit("allCompletesDeliveries", res.data)
      } else {
        commit("setTodaysDeliveries", res.data)
      }
      return true
    })
    .catch(error => {
      return false
    });
  },

  updateCustomOrderDeliveryStatus ({ commit }, {id, delivery_status}) {
    return securedApi.post(`${pantryApi}/custom_deliveries/update_custom_delivery_status/${id}/${delivery_status}`, {})
    .then(res => {
      commit("updateOrderStatus", res.data)
      return true
    })
    .catch(error => {
      return false
    });
  },

  postNewCustomClient({ commit }, custom_client) {
    return securedApi.post(`${pantryApi}/custom_clients`, custom_client)
    .then(res => {
      commit("setCustomClients", res.data)
      return true
    })
    .catch(error => {
      return false
    });
  },

  postNewCustomDelivery({ commit }, custom_delivery) {
    return securedApi.post(`${pantryApi}/custom_deliveries`, custom_delivery)
    .then(res => {
      if (custom_delivery.route == "/my_deliveries") {
        commit("setMyDeliveryOrders", res.data);
      } else if (custom_delivery.route == "/upcoming_deliveries") {
        commit("allUpcomingOrders", res.data)
      } else if (custom_delivery.route == "/todays_deliveries"){
        commit("setTodaysDeliveries", res.data)
      }
      return true
    })
    .catch(error => {
      return false
    });
  },


  fetchOrderWaybills({ commit }) {
    return securedApi.get(`${pantryApi}/order_waybills`, {})
    .then(res => {
      commit("setOrderWaybills", res.data);
      return true
    })
    .catch(error => {
      return false
    });
  },

  getCustomClients({ commit }, supplier_id) {
    return securedApi.get(`${pantryApi}/custom_clients/company_custom_clients/${supplier_id}`, {})
    .then(res => {
      commit("setCustomClients", res.data)
      return true
    })
    .catch(error => {
      return false
    });
  },

  // fetchUserDeliveryOrders({ commit }) {
  //   return securedApi.get(`${pantryApi}/user_deliverable_orders`, {})
  //   .then(res => {
  //     if (res) {
  //       commit("setMyDeliveryOrders", res.data);
  //       return true
  //     }
  //   })
  //   .catch((err) => {
  //     return false
  //   })
  // },

  fetchUserDailyDeliveryOrders({ commit }, {userId, companyId}) {
    return securedApi.get(`${pantryApi}/user_daily_deliverable_orders/${userId}/${companyId}`, {})
    .then(res => {
      if (res) {
        commit("setMyDeliveryOrders", res.data);
        return true
      }
    })
    .catch((err) => {
      return false
    })
  },

  updateOrderDeliveryStatus({ commit }, { order_id, status }) {
    return securedApi
    .post(`${pantryApi}/update_order_delivery_status/${order_id}`, {
      delivery_status: status
    })
    .then(res => {
      commit("updateOrderStatus", res.data);
      return true;
    })
    .catch(() => {
      return false;
    });
  },

  claimCredit({ commit, state }, credit_note) {
    return securedApi
      .post(`${pantryApi}/credit_notes`, {
        credit_note
      })
      .then(() => {
        return true;
      })
      .catch(() => {
        return false;
      });
  },

  newOrder({ dispatch }, order) {
    return securedApi
      .post(`${pantryApi}/order_with_credit_note`, order, {})
      .then(function(response) {
        return response
      })
  },

  receiveOrder({ commit }, id) {
    return securedApi.get(`${pantryApi}/receive_order/${id}`, {}).then(res => {
      if (res.data.type !== "error") {
        commit("setCurrentReceiveOrder", res.data);
        return res;
      } else {
        return false;
      }
    });
  },

  orderSignature({ commit }, { data, order_id, received_by, delivery_note }) {
    return securedApi
      .post(`${pantryApi}/order_signature/${order_id}`, {
        signature: data,
        received_by: received_by,
        delivery_note: delivery_note
      })
      .then(res => {
        if (res.status) {
          return true;
        }
      })
      .catch(() => {
        return false;
      });
  },


  updateReceivedOrderAndValues({ commit }, order) {
    return securedApi
      .post(`${pantryApi}/update_order_received_values/${order.order_uuid}`, {
        order
      })
      .then(res => {
        if (res.status == 200) {
          return true;
        } else {
          throw res; 
        }
      })
      .catch((err) => {
        throw err; 
      });
  },

}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
