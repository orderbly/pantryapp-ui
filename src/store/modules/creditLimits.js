import axios from 'axios'
import { securedAxiosInstance, plainAxiosInstance } from '../../axios/index' 
import { pantryApi } from '@/constants/config'
import { apiHandler } from '../../util/apiHandler';
import { getCookie, setCookie } from '../../constants/cookies'

let plainApi = plainAxiosInstance
let securedApi = securedAxiosInstance
const isLoggedin = function () {
  return getCookie('access') !== undefined ? true : false
}

const state = {
  avaialbleCompanies: [],
  creditLimits: [],
  accountBalance: 0,
  accountLimit: 0,
  availableBalance: 0
}

const getters = {
  avaialbleCompanies: state => state.avaialbleCompanies,
  creditLimits: state => state.creditLimits,
  accountBalance: state => state.accountBalance,
  accountLimit: state => state.accountLimit,
  availableBalance: state => state.availableBalance
}

const mutations = {
  saveAvaialbleCompanies(state, data) {
    state.avaialbleCompanies = data
  },
  removeCreditLimit(state, id) {
    state.creditLimits = state.creditLimits.filter(cl => cl.id !== id)
  },
  
  updateCreditLimit(state, credit_limit) {
    state.creditLimits.forEach(cl => {
      if (cl.id === credit_limit.id) {
        cl.credit_limit = credit_limit.credit_limit
      }
    })
  },

  saveCreditLimits(state, data) {
    state.creditLimits = data
  },
  saveAddedCrediLimit(state, data) {
    state.creditLimits.push(data)
  },
  setAccountBalances(state, data) {
    state.accountBalance = data.balance
    state.accountLimit = data.credit_limit
    state.availableBalance = data.available_balance
  },
}

const actions = {
  fetchAllCreditLimits({ commit }, company_id) {
    return securedApi.get(`${pantryApi}/credit_limits/company_credit_limits/${company_id}`, {})
    .then(response => { 
      commit('saveCreditLimits', response.data)
      return true
    })
    .catch(() => {
      return false
    })
  },

  fetchCreditLimitAvailableCompanies({ commit }, company_id) {
    return securedApi.get(`${pantryApi}/credit_limits/available_companies/${company_id}`, {})
    .then(response => { 
      commit('saveAvaialbleCompanies', response.data)
      return true
    })
    .catch(() => {
      return false
    })
  },
  
  fetchCreditBalance({ commit }, {supplier_id, customer_id}) {
    return securedApi.get(`${pantryApi}/credit_limits/account/${supplier_id}/${customer_id}`, {})
    .then(response => {
      commit('setAccountBalances', response.data)
      return true
    })
    .catch(() => {
      return false
    })
  },

  newCreditLimit({ commit }, {supplier_id, customer_id, credit_limit}) {
    let creditLimit = {credit_limit: {supplier_id, customer_id, credit_limit: parseFloat(credit_limit) }}
    return securedApi.post(`${pantryApi}/credit_limits`, creditLimit)
    .then(response => {
      commit('saveAddedCrediLimit', response.data)
      return true
    })
    .catch(() => {
      return false
    })
  },

  updateCreditLimit({ commit }, {supplier_id, customer_id, credit_limit, id}) {
    let creditLimit = {credit_limit: {supplier_id, customer_id, credit_limit: parseFloat(credit_limit) }}
    return securedApi.patch(`${pantryApi}/credit_limits/${id}`, creditLimit)
    .then(response => {
      commit('updateCreditLimit', response.data)
      return true
    })
    .catch(() => {
      return false
    })
  },

  deleteCreditLimit({ commit }, id) {
    return securedApi.delete(`${pantryApi}/credit_limits/${id}`)
    .then(response => {
      commit('removeCreditLimit', id)
      return true
    })
    .catch(() => {
      return false
    })
  }

}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
