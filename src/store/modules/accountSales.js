import { securedAxiosInstance } from '../../axios/index' 
import { pantryApi } from '@/constants/config'
import { getCookie } from '../../constants/cookies'

let securedApi = securedAxiosInstance
const isLoggedin = function () {
  return getCookie('access') !== undefined ? true : false
}

const state = {
  supplierAgeAnalysis: [],
  customerAgeAnalysis: [],
  supplierStatement: [],
  supplierAgeAnalysisBalance: [],
  customerAgeAnalysisBalance: [],
  customerStatement: [],
  customerAnalysis: [],
  supplierAnalysis: []
}

const getters = {
  supplierAgeAnalysis: state => state.supplierAgeAnalysis,
  supplierStatement: state => state.supplierStatement,
  customerAgeAnalysis: state => state.customerAgeAnalysis,
  customerStatement: state => state.customerStatement,
  supplierAgeAnalysisBalance: state => state.supplierAgeAnalysisBalance,
  customerAgeAnalysisBalance: state => state.customerAgeAnalysisBalance,
  customerAnalysis: state => state.customerAnalysis,
  supplierAnalysis: state => state.supplierAnalysis
}

const mutations = {

  setSupplierAgeAnalysis(state, data) {
    let keys = Object.keys(data)
    let object = []
    keys.forEach(k =>  object.push({supplier_title: k, supplier_id: data[k][0].supplier_id, '0-30 Days': 0, '30-60 Days': 0, '60-90 Days': 0}))
    
    var future = new Date();
    let thirtyDays = future.setDate(future.getDate() + 30);
    let thirtyDaysBalance = 0
    let sixtyDays = future.setDate(future.getDate() + 60);
    let sixtyDaysBalance = 0
    let ninetyDays = future.setDate(future.getDate() + 90);
    let ninetyDaysBalance = 0

    keys.forEach(k => {
      let index = object.findIndex(l => l['supplier_title'] === k)
      data[k].forEach((a,i) => {
        if (Date.parse(a.created_at) <= thirtyDays) {
          object[index]['0-30 Days'] += a.value
          thirtyDaysBalance += a.value
        }
        else if (Date.parse(a.created_at) >= thirtyDays && Date.parse(a.created_at) <= sixtyDays)  {
          object[index]['30-60 Days'] += a.value
          sixtyDaysBalance += a.value
        } 
        else if ((Date.parse(a.created_at) >= sixtyDays && Date.parse(a.created_at) <= ninetyDays)) {
          object[index]['60-90 Days'] += a.value
          ninetyDaysBalance += a.value
        }
      })  
    })
    state.supplierAgeAnalysis = object
    state.supplierAgeAnalysisBalance = [thirtyDaysBalance, sixtyDaysBalance, ninetyDaysBalance]
  },

  setCustomerAgeAnalysis(state, data) {
    let keys = Object.keys(data)
    let object = []
    keys.forEach(k =>  object.push({customer_title: k, customer_id: data[k][0].customer_id, '0-30 Days': 0, '30-60 Days': 0, '60-90 Days': 0}))

    var future = new Date();
    let thirtyDays = future.setDate(future.getDate() + 30);
    let sixtyDays = future.setDate(future.getDate() + 60);
    let ninetyDays = future.setDate(future.getDate() + 90);
    let thirtyDaysBalance = 0
    let sixtyDaysBalance = 0
    let ninetyDaysBalance = 0

    keys.forEach(k => {
      let index = object.findIndex(l => l['customer_title'] === k)
      data[k].forEach((a,i) => {
        if (Date.parse(a.created_at) <= thirtyDays) {
          object[index]['0-30 Days'] += a.value
          thirtyDaysBalance += a.value
        }
        else if (Date.parse(a.created_at) >= thirtyDays && Date.parse(a.created_at) <= sixtyDays)  {
          object[index]['30-60 Days'] += a.value
          sixtyDaysBalance += a.value
        } 
        else if ((Date.parse(a.created_at) >= sixtyDays && Date.parse(a.created_at) <= ninetyDays)) {
          object[index]['60-90 Days'] += a.value
          ninetyDaysBalance += a.value
        }
      })  
    })
    state.customerAgeAnalysis = object
    state.customerAgeAnalysisBalance = [thirtyDaysBalance, sixtyDaysBalance, ninetyDaysBalance]
  },

  setSupplierStatement(state, data) {
    state.supplierStatement = data
  },

  setCustomerStatement(state, data) {
    state.customerStatement = data
  },

  setCustomerAnalysis(state, data) {
    state.customerAnalysis = data
  },
  
  setSupplierAnalysis(state, data) {
    state.supplierAnalysis = data
  }
}

const actions = {

  fetchSupplierAgeAnalysis({ commit, store }, id) {
    if (!isLoggedin()) {
      return false
    }
    securedApi.get(`${pantryApi}/supplier_age_analysis`, {})
    .then(response => { 
      if (response.data) {
        commit('setSupplierAgeAnalysis', response.data)
      }
    })
  },
  
  fetchSupplierStatement({ commit, store }, id) {
    if (!isLoggedin()) {
      return false
    }
    securedApi.get(`${pantryApi}/supplier_statement/${id}`, {})
    .then(response => { 
      if (response.data) {
        commit('setSupplierStatement', response.data)
      }
    })
  },

  fetchCustomerStatement({ commit, store }, id) {
    if (!isLoggedin()) {
      return false
    }
    securedApi.get(`${pantryApi}/customer_statement/${id}`, {})
    .then(response => { 
      if (response.data) {
        commit('setCustomerStatement', response.data)
      }
    })
  },

  fetchCustomerAgeAnalysis({ commit }, id) {
    if (!isLoggedin()) {
      return false
    }
    securedApi.get(`${pantryApi}/customer_age_analysis`, {})
    .then(response => { 
      if (response.data) {
        commit('setCustomerAgeAnalysis', response.data)
      }
    })
  },

  fetchCustomerAnalysis({ commit }, id) {
    if (!isLoggedin()) {
      return false
    }
    securedApi.get(`${pantryApi}/customer_analysis`, {})
    .then(response => { 
      if (response.data) {
        commit('setCustomerAnalysis', response.data)
      }
    })
  },

  fetchSupplierAnalysis({ commit }) {
    if (!isLoggedin()) {
      return false
    }
    securedApi.get(`${pantryApi}/supplier_analysis`, {})
    .then(response => { 
      if (response.data) {
        commit('setSupplierAnalysis', response.data)
      }
    })
  },
  
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
