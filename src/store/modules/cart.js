import { securedAxiosInstance, plainAxiosInstance } from '../../axios/index' 
import { pantryApi } from '../../constants/config'
import { apiHandler } from '../../util/apiHandler';

let plainApi = plainAxiosInstance
let securedApi = securedAxiosInstance

const state = {
  cart: [],
  activeAddressId: null,
  deliveryDate: Date.apply(),
  items: [],
  cartProductVariants: [],
  activeSupplier: null,
  multipleSupplier: false,
  order_items: [],
  deliveryFee: 0,
  minimumOrderValue: {},
  orderNote: ""
}

const getters = {
  getDeliveryFee: state => state.deliveryFee,
  getActiveSupplier: state => state.activeSupplier,
  getDeliveryDate: state => state.deliveryDate,
  getCart: state => state.cart,
  getCartLength: state => state.cart.length,
  getMinimumOrderValue: state => state.minimumOrderValue,
  cartTotalQuantity: (state) => {
    let quantity = 0
    state.cart.map(c => (quantity += c.quantity))
    return quantity
  },
  cartTotal(state) {
    if (state.cart.length > 0) {
      let total = 0
      state.cart.map(
        c => (total += c.quantity * (c.unit_amount + c.tax_amount))
      )
      return total
    } else {
      return 0
    }
  },
  vatTotal(state) {
    if (state.cart.length > 0) {
      let total = 0
      state.cart.map(c => (total += c.quantity * c.tax_amount))
      return total
    } else {
      return 0
    }
  },

  cartProducts(state) {
    let items = []
    if (state.cart.length > 0) {
      state.cart.map(i => {
        items.push({
          id: i.product_variant_id,
          title: i.product_variant + ' ' + i.product_name,
          quantity: i.quantity,
          price: i.unit_amount + i.tax_amount,
          available_stock: i.available_stock,
          track_stock: i.track_stock,
          product_id: i.product_id,
        })
      })
    }
    return items
  }
}

const mutations = {

  saveOrderNote (state, note) {
    state.orderNote = note
  },

  saveActiveOrder(state, id) {
    state.a = state.cart.filter(i => i.product_variant_id !== id)
  },

  clearCart(state) {
    state.activeSupplier = null
    state.cart = []
    state.cartProductVariants = []
  },

  updateCartContent(state, id) {
    state.cart = state.cart.filter(i => i.product_variant_id !== id)
    state.cartProductVariants = state.cartProductVariants.filter(pv => pv !== id)
    if (state.cart.length == 0) {
      state.activeSupplier = null
      state.minimumOrderValue = {}
    }
  },

  setOrderDate(state, date) {
    state.deliveryDate = date
  },

  setCartAddress(state, id) {
    state.activeAddressId = id
  },

  setDeliveryFee(state, data) {
    state.deliveryFee = data
  },

  setProductQuantity(state, props) {
    state.cart.map(i => {
      if (i.product_variant_id === props.product_variant_id) {
        i.quantity = props.quantity
      }
    })
  },

  addProductToCart(state, item) {
    localStorage.setItem('cart', [])
    if (
      state.activeSupplier == null ||
      state.activeSupplier === item.company_id
    ) {
      if(state.cart.length > 0 && state.cartProductVariants.includes(item.product_variant_id)) {
        state.cart.forEach((s) => {
          if (item.product_variant_id === s.product_variant_id && item.product_id === s.product_id) {
            s.quantity = parseInt(item.quantity) + parseInt(s.quantity)
          }
        })
      } else {
        state.cartProductVariants.push(item.product_variant_id)
        state.activeSupplier = item.company_id
        state.cart.push(item)
        localStorage.removeItem('cart');
        localStorage.setItem('cart', JSON.stringify(state.cart));
      }
    }
  },
  setMinimumOrderValue(state, data) {
    state.minimumOrderValue = data
  },
}

const actions = {
  deleteProductFromCart({ commit }, id) {
    commit('updateCartContent', id)
    return true
  },

  changeCartProductQuantity({ commit, state }, props) {
    commit('setProductQuantity', props)
  },
  saveOrderDate({ commit, state }, props) {
    commit('setOrderDate', props)
  },

  addBackStock({commit, state}, product_variant_id) {
    let cartItem = state.cart.find(ci => ci.product_variant_id === product_variant_id)
    if (cartItem != null && cartItem != undefined) {
      commit('products/updateProductVariantStock', cartItem, { root: true })
    }
  },

  modifyQuickSelectCart ({commit}, item) {
    commit('addProductToCart', item)
    commit('products/updateQuickSelectProductVariantQuantity', item, { root: true })
  },

  modifyCart({ commit } , item) {
    commit('addProductToCart', item)
    commit('products/updateProductVariantQuantity', item, { root: true })
  },

  checkoutCart({ dispatch, commit, rootState }) {
    let backupCart = state.cart

    state.cart.forEach((cart) =>  {
      cart['gross_price'] = parseFloat(cart.tax_amount) + parseFloat(cart.unit_amount)
      cart['vat_price'] = cart.tax_amount
      cart['net_price'] = cart.unit_amount
      cart['company_id'] = rootState.companies.current_company.id
      cart['delivery_date'] = rootState.cart.deliveryDate
      delete cart.unit_amount
      delete cart.track_stock
      delete cart.available_stock
      delete cart.tax_amount
      delete cart.product_name
      delete cart.product_variant
    })

    const order = {
      delivery_date: state.deliveryDate,
      notes: rootState.cart.orderNote,
      supplier_id: rootState.cart.activeSupplier,
      delivery_address_id: rootState.cart.activeAddressId,
      order_items_attributes: state.cart
    }

    return securedApi.post(`${pantryApi}/orders`, {
        order
      }, {})
      .then(function(response) {
        if (response.status === 201) {
          commit('clearCart')
          commit('saveActiveOrder', response.data)
          dispatch('orders/fetchSalesOrders', null, { root: true } )
          dispatch('orders/fetchPendingSalesOrders', null, { root: true } )
          return response
        }
      })
      .catch((err) => {
        return err
      })
  },

  postCustomOrder ({commit}, order) {
    return securedApi.post(`${pantryApi}/create_custom_order`, order)
    .then(res=> res)
    .catch(err => err)
  },

  payNow({ dispatch, commit, rootState }) {
    let backupCart = state.cart
    state.cart.forEach((cart) =>  {
      cart['gross_price'] = parseFloat(cart.tax_amount) + parseFloat(cart.unit_amount)
      cart['vat_price'] = cart.tax_amount
      cart['net_price'] = cart.unit_amount
      cart['company_id'] = rootState.companies.current_company.id
      delete cart.unit_amount
      delete cart.track_stock
      delete cart.available_stock
      delete cart.tax_amount
      delete cart.product_name
      delete cart.product_variant
    })

    const order = {
      delivery_date: state.deliveryDate,
      notes: null,
      supplier_id: rootState.cart.activeSupplier,
      order_items_attributes: state.cart
    }

    return securedApi.post(`${pantryApi}/orders`, { order }, {})
    .then(function(response) {
      if (response.status === 201 || response.status === 200 ) {
        rootState.cart.cart = []
        commit('saveActiveOrder', response.data)
        dispatch('orders/saveActiveOrder', response.data, { root: true } )
        dispatch('payGate/selectActiveOrder', response.data, { root: true } )
        dispatch('orders/fetchPendingSalesOrders', null, { root: true } )
        return true
      }
    })
    .catch(() => {
      return false
    })
  },

  calculateDeliveryFee({commit, state, getters}, addressId) {
    if (addressId && activeSupplier !== null) {
      return securedApi.get(`${pantryApi}/delivery_fee_brackets/calculate_delivery_fee/${addressId}/${state.activeSupplier}`)
      .then((res) => {
        commit('setDeliveryFee', res.data)
      })
    }
  },
  fetchMinimumOrderValue({ commit }, supplierId) {
    return securedApi.get(`${pantryApi}/companies/fetch_minimum_order_value/${supplierId}`)
    .then(response => {
      commit( 'setMinimumOrderValue', response.data)
      return response
    })
    .catch(error => error)
  },
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}

