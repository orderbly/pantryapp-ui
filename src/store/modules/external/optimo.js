import { securedAxiosInstance } from '../../../axios/index' 
import { pantryApi} from '@/constants/config'

let securedApi = securedAxiosInstance

const actions = {
  createDelivery({ commit }, {order_uuid, company_uuid}) {
    return securedApi.post(`${pantryApi}/integrations/optimo/create_delivery/${order_uuid}/${company_uuid}`, {})
    .then(response => {
      commit('orders/updateOptimoSync', order_uuid, {root: true})
      return response
    })
    .catch(error =>  {
      return error
    })
  }
}

export default {
  namespaced: true,
  actions
}
