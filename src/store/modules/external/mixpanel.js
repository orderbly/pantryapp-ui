import Mixpanel from 'mixpanel'
import { isProduction } from '../../../constants/config'
import { plainAxiosInstance } from '../../../axios/index' 
import platform from 'platform'
let mixPanelToken = isProduction ? "78edbe2c14ba13bff482c3bfd9f74b5a" : "f8facd016203a292d99a98d863cd2d9e"
var mixpanel = Mixpanel.init(mixPanelToken);

const actions = {
  async mixpanelTrack ({state},{track, obj}) {
    await plainAxiosInstance.get('https://freegeoip.app/json/', {}).then((res) => {
      if (platform.description) obj.$browser_version = platform.description
      if (platform.product) obj.$model = platform.product
      if (platform.os && platform.os.family) obj.$os = platform.os.family
      obj.$country = res.data.country_name
      obj.$city = res.data.city
      obj.mp_country_code = res.data.country_code
      obj.lat = res.data.latitude
      obj.lon = res.data.longitude
      obj.zip_code = res.data.zip_code
      obj.query = res.data.ip
      obj.regionName = res.data.region_name
    })
    mixpanel.track(track, obj)
  }
}

export default {
  namespaced: true,
  actions
}
