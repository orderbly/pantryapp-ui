import { notifyUrl, ozowApiKey, ozowPrivateKey, testingEnv } from '../../../constants/config';
import { getCookie } from '../../../constants/cookies'
import { startOfSecond } from 'date-fns';

const state = {
  activeOrder: {
    private_key: ozowPrivateKey, // payfastMerchantId
    api_key: null, // payfastMerchantKey
    notify_url: notifyUrl, // notifyUrl
    order: null,
    customer_name: null, // accountSale.customer_name
    amount: null,
    supplier_name: null, // accountSale.supplier_name
    order_id: null, // accountSale.order_id
    supplier_id: null, // 'order number'
    payment_type: null, // 'account_sale' || order
    customer_id: null, // 'current company id'
    order_number: null, // 'order_number'
  },

  //PayGate
  displayPayGateModal: false
}

const getters = {
  activeOrder: state => state.activeOrder.order,
  private_key: state => ozowPrivateKey,
  testingEnv: state => testingEnv,
  api_key: state => state.activeOrder.api_key,
  notify_url: state => state.activeOrder.notify_url,
  customer_name: state => state.activeOrder.customer_name,
  amount: state => state.activeOrder.amount,
  supplier_name: state => state.activeOrder.supplier_name,
  order_id: state => state.activeOrder.order_id,
  supplier_id: state => state.activeOrder.supplier_id,
  payment_type: state => state.activeOrder.payment_type,
  customer_id: () => JSON.parse(getCookie('company')).id,
  order_number: () =>  state.activeOrder.order_number,

  getPayGateModal: state => state.displayPayGateModal,
}

const mutations = {

  updateActiveAccountSale (state, account) {
    state.activeOrder.order = null
    state.activeOrder.order = account
    state.activeOrder.private_key = ozowPrivateKey
    state.activeOrder.api_key = ozowApiKey
    state.activeOrder.notify_url = notifyUrl
    state.activeOrder.customer_name = account.customer
    state.activeOrder.amount = account.gross_amount
    state.activeOrder.supplier_name = account.supplier.title
    state.activeOrder.order_id = ""
    state.activeOrder.supplier_id = account.supplier.id
    state.activeOrder.payment_type = 'account_sale'
    state.activeOrder.customer_id = account.customer_id
  },

  updateActiveOrder(state, order) {
    state.activeOrder.order = null;
    state.activeOrder.order = order
    state.activeOrder.private_key = ozowPrivateKey
    state.activeOrder.api_key = ozowApiKey
    state.activeOrder.notify_url = notifyUrl
    state.activeOrder.customer_name = order.customer.title
    state.activeOrder.amount = order.balance.toFixed(2);
    state.activeOrder.supplier_name = order.supplier.title
    state.activeOrder.order_id = order.id
    state.activeOrder.supplier_id = order.supplier.id
    state.activeOrder.payment_type = 'order'
    state.activeOrder.customer_id = order.customer.id
    state.activeOrder.order_number = order.order_number
  },

  clearActiveOrder(state) {
    state.activeOrder.order = null;
    state.activeOrder.private_key = null;
    state.activeOrder.api_key = null;
    state.activeOrder.notify_url = null;
    state.activeOrder.customer_name = null;
    state.activeOrder.amount = null;
    state.activeOrder.supplier_name = null;
    state.activeOrder.order_id = null;
    state.activeOrder.supplier_id = null;
    state.activeOrder.payment_type = null;
    state.activeOrder.customer_id = null;
    state.activeOrder.order_number = null;
  },

  setPayGateModal(state) {
    state.displayPayGateModal = !state.displayPayGateModal
  },
}

const actions = {
  togglePayGateModal({ commit }) {
    commit('setPayGateModal')
  },

  selectActiveOrder({ commit, dispatch }, order) {
    commit('updateActiveOrder', order)
    setTimeout(() => { 
      commit('setPayGateModal')
    }, 1500 )
  },
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
