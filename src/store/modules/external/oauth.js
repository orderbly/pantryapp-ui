import axios from 'axios'
import { securedAxiosInstance, plainAxiosInstance } from '../../../axios/index' 
import { pantryApi } from '@/constants/config'
import { apiHandler } from '../../../util/apiHandler';
import { getCookie } from '../../../constants/cookies'

let plainApi = plainAxiosInstance
let securedApi = securedAxiosInstance
const isLoggedin = function () {
  return getCookie('access') !== undefined ? true : false
}

const state = {
  authorize_url: null,
  displayOauthModal: false,
}

const getters = {
  authorizeUrl: state => state.authorize_url,
  getOauthModal: state => state.displayOauthModal,
}

const mutations = {
  setAuthorizeUrl(state, data) {
    state.authorize_url = data
  },

  clearAuthorizeUrl(state) {
    state.authorize_url = null
  },

  setOauthModal(state) {
    state.displayOauthModal = !state.displayOauthModal
  },
}

const actions = {
  fetchAuthorizeUrl({ commit, state }, {provider, companyId}) {
    if (!isLoggedin()) {
      return false
    }
    securedApi.get(`${pantryApi}/integrations/${provider}/auth/authorize_uri/${companyId}`, {})
    .then(response => { 
      if (response.data) {
        commit('setAuthorizeUrl', response.data)
      }
    })
  },

  toggleOauthModal({ commit }) {
    commit('setOauthModal')
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
