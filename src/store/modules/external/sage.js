import axios from 'axios'
import { securedAxiosInstance } from '../../../axios/index' 
import { pantryApi, validateSageLoginUrl, sageApiKey } from '@/constants/config'
import { getCookie, setCookie } from '../../../constants/cookies'

let securedApi = securedAxiosInstance
const isLoggedin = function () {
  return getCookie('access') !== undefined ? true : false
}

const state = {
  configurations: [],
  accounts: [],
  items: [],
  contacts: [],
  apiResponse: {},
  sageIntegrationModal: false,
  sageCompanySelectModal: false,
  sageCompanies: [],
  sageUserCrededentials: null,
  sageConfigurationModal: false,
  accountDefaults: {},
}

const getters = {
  allConfigurations: state => state.configurations,
  allAccounts: state => state.accounts,
  accountDefaults: state => state.accountDefaults,
  allContacts: state => state.contacts,
  allItems: state => state.items,
  apiResponse: state => state.apiResponse,
  getSageIntegrationModal: state => state.sageIntegrationModal,
  getSageCompanies: state => state.sageCompanies,
  getSageCompanySelectModal: state => state.sageCompanySelectModal,
  getSageConfigurationModal: state => state.sageConfigurationModal
}

const mutations = {

  setSageCompanies: (state, data) => state.sageCompanies = data,
  setSageUserCrededentials: (state, data) => state.sageUserCrededentials = data,
  setSageUserCrededentials: (state, data) => state.sageUserCrededentials = data,
  setConfigurations: (state, data) => state.configurations = data,  
  setSageConfigurations: (state, data) =>  state.configurations = data,
  setAccounts (state, data) {
    state.accounts = data.accounts
    state.accountDefaults = data.defaults
  },
  setItems: (state, data) => state.items = data,
  setContacts: (state, data) => state.contacts = data,
  setApiResponse: (state, data) => state.apiResponse = data,

  toggleSageIntegrationModal: state => state.sageIntegrationModal = !state.sageIntegrationModal,
  toggleSageCompanySelectModal: state => state.sageCompanySelectModal = !state.sageCompanySelectModal,
  toggleSageConfigurationModal: state => state.sageConfigurationModal = !state.sageConfigurationModal,
}

const actions = {
  validateSage({ commit }, {username, password}) {
    return axios.post(validateSageLoginUrl, {
      username, password
    })
    .then(response => { 
      return response.data
    })
  },

  fetchSageAccount ({commit, state}, companyId) {
    securedApi.post(`${pantryApi}/integrations/sage/create`, payload)
    .then(response => { 
      if (response.data) {
        commit('setIntegrationProviders', response.data)
      }
    })
  },

  fetchSageConfigurations({ commit }, company_id) {
    securedApi.get(`${pantryApi}/integrations/sage/configurations/${company_id}`, {})
    .then(response => { 
      commit('setSageConfigurations', response.data)
      return true
    })
    .catch(error =>  {
      return false
    })
  },


  fetchAccounts({ commit }, company_uuid) {
    return securedApi.get(`${pantryApi}/integrations/sage/accounts/${company_uuid}`, {})
    .then(response => { 
        commit('setAccounts', response.data)
        return true
      })
      .catch(error =>  {
        return false
    })
  },

  fetchAllItems({ commit }, comanyUuid) {
    securedApi.get(`${pantryApi}/integrations/sage/items/${comanyUuid}`, {})
    .then(response => { 
      if (response.data) {
        commit('setItems', response.data)
      }
    })
    .catch(error =>  {
      commit('setApiResponse', error.response.data)
    })
  },

  fetchContacts({ commit }) {
    return securedApi.get(`${pantryApi}/integrations/xero/contacts`, {})
    .then(response => { 
      if (response.data) {
        commit('setContacts', response.data)
        return true
      }
    })
    .catch(error =>  {
      return true
    })
  },

  fetchSageSuppliers({ commit }, company_uuid) {
    return securedApi.get(`${pantryApi}/integrations/sage/contacts/suppliers/${company_uuid}`, {})
    .then(response => { 
      if (response.data) {
        commit('setContacts', response.data)
        return response
      }
    })
    .catch(error =>  {
      return error
    })
  },

  fetchSageCustomers({ commit }, company_uuid) {
    return securedApi.get(`${pantryApi}/integrations/sage/contacts/customers/${company_uuid}`, {})
    .then(response => { 
      if (response.data) {
        commit('setContacts', response.data)
        return response
      }
    })
    .catch(error =>  {
      return error
    })
  },

  createSagePurchaseOrder({ commit, dispatch }, { orderId, sageOneSupplierId }) {
    return securedApi.post(`${pantryApi}/integrations/sage/orders/purchase_order/${orderId}/${sageOneSupplierId}`, {})
    .then(response => {
      commit('orders/updateIntegratedOrder', response.data, {root: true})
      return response
    })
    .catch(error =>  {
      return error
    })
  },

  createSagePurchaseReturn({ commit }, { creditNoteUuid, sageOneSupplierId }) {
    return securedApi.post(`${pantryApi}/integrations/sage/orders/purchase_return/${creditNoteUuid}/${sageOneSupplierId}`, {})
    .then(response => {
      commit('orders/updateIntegratedOrder', response.data, {root: true})
      return response
    })
    .catch(error =>  {
      return error
    })
  },
  
  createSageSalesOrder({ commit, dispatch }, { orderId, sageOneSupplierId }) {
    return securedApi.post(`${pantryApi}/integrations/sage/orders/sales_order/${orderId}/${sageOneSupplierId}`, {})
    .then(response => {
      commit('orders/updateIntegratedOrder', response.data, {root: true})
      return response
    })
    .catch(error =>  {
      return error
    })
  },

  createSageSalesReturn({ commit }, { creditNoteUuid, sageOneCustomerId }) {
    return securedApi.post(`${pantryApi}/integrations/sage/orders/sales_return/${creditNoteUuid}/${sageOneCustomerId}`, {})
    .then(response => {
      commit('orders/updateIntegratedOrder', response.data, {root: true})
      return response
    })
    .catch(error =>  {
      return error
    })
  },

  saveDefaults({ commit, dispatch }, { account, uuid }) {
    return securedApi.post(`${pantryApi}/integrations/sage/accounts/savedefaults/${uuid}`, { account })
    .then(response => {
      return response
    })
    .catch(error =>  {
      return error
    })
  },

  disconnectSage({commit}, company_uuid) {
    return securedApi.post(`${pantryApi}/integrations/sage/disconnect/${company_uuid}`, {})
    .then(response => {
      return response
    })
    .catch(error =>  {
      return error
    })
  },

  // saveContact({ commit, dispatch }, { contact } ) {
  //   if (!isLoggedin()) {
  //     return false
  //   }
  //   return securedApi.post(`${pantryApi}/integrations/xero/contacts/savecontact`, { contact })
  //   .then(response => {
  //     commit('setApiResponse', {type: 'message', message: 'ok'})
  //     return true
  //   })
  //   .catch(error =>  {
  //     commit('setApiResponse', error.response.data)
  //   })
  // },

  // saveCreditNoteContact({ commit, dispatch }, { contact } ) {
  //   if (!isLoggedin()) {
  //     return false
  //   }
  //   return securedApi.post(`${pantryApi}/integrations/xero/contacts/save_credit_note_contact`, { contact })
  //   .then(response => {
  //     commit('setApiResponse', {type: 'message', message: 'ok'})
  //     return true
  //   })
  //   .catch(error =>  {
  //     commit('setApiResponse', error.response.data)
  //   })
  // },
  saveSageCompanyIntegration ({commit, state}, {companyId, currentCompany}) {
    let payload = {
      company_id: companyId,
      current_company: currentCompany,
      base64_sage_details: state.sageUserCrededentials,
    }
    return securedApi.post(`${pantryApi}/integrations/sage/create`, payload)
    .then(response => { 
      if (response.data) {
        commit('setIntegrationProviders', response.data)
        return response
      }
    })
    .catch((err) => {
      return err
    })
  },

  sync({ commit }, company_uuid) {
    return securedApi.post(`${pantryApi}/integrations/sage/sync/${company_uuid}`, {})
    .then(response => { 
      if (response.data) {
        commit('setSageConfigurations', response.data)
        return response
      }
    })
    .catch((err) => {
      return err
    })
  },

  getSageCompanies({ commit }, secureDetails) {
    commit('setSageUserCrededentials', secureDetails)
    const secureSageApi = axios.create({
      baseURL: `https://resellers.accounting.sageone.co.za/api/2.0.0/Company/Get?apikey={${sageApiKey}}`,
      timeout: 1000,
      headers: {
        'Authorization': 'Basic ' + secureDetails,
        'content-type': 'application/json'
      }
    });

    return secureSageApi.get('', {})
    .then(response => { 
      commit('setSageCompanies', response.data.Results)
      return response
    })
    .catch(err => err)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
