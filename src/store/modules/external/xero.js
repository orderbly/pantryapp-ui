import { securedAxiosInstance } from '../../../axios/index' 
import { pantryApi } from '@/constants/config'
import { getCookie } from '../../../constants/cookies'

let securedApi = securedAxiosInstance
const isLoggedin = function () {
  return getCookie('access') !== undefined ? true : false
}

const state = {
  configurations: [],
  accounts: [],
  items: [],
  contacts: [],
  apiResponse: {}
}

const getters = {
  allConfigurations: state => state.configurations,
  allAccounts: state => state.accounts,
  allContacts: state => state.contacts,
  allItems: state => state.items,
  apiResponse: state => state.apiResponse
}

const mutations = {
  setConfigurations(state, data) {
    state.configurations = data
  },
  
  setSageConfigurations(state, data) {
    state.configurations = data
  },

  setAccounts(state, data) {
    state.accounts = data
  },

  setItems(state, data) {
    state.items = data
  },

  setContacts(state, data) {
    state.contacts = data
  },

  setApiResponse(state, data) {
    state.apiResponse = data
  }
}

const actions = {
  disconnectXero({commit}, company_uuid) {
    securedApi.post(`${pantryApi}/integrations/xero/disconnect/${company_uuid}`, {})
    .then(response => { 
      if (response.data) {
        return response
      }
    })
    .catch(error =>  {
      return error
    })
  },
  fetchConfigurations({ commit }) {
    securedApi.get(`${pantryApi}/integrations/xero/configurations/`, {})
    .then(response => { 
      if (response.data) {
        commit('setConfigurations', response.data)
        commit('setApiResponse', {type: 'message', message: 'ok'})
      }
    })
    .catch(error =>  {
      commit('setApiResponse', error.response.data)
    })
  },

  fetchSageConfigurations({ commit }, company_id) {
    securedApi.get(`${pantryApi}/integrations/sage/configurations/${company_id}`, {})
    .then(response => { 
      commit('setSageConfigurations', response.data)
      return true
    })
    .catch(error =>  {
      return false
    })
  },

  syncConfigurations({ state, dispatch }) {
    if (!isLoggedin()) {
      return false
    }
    return securedApi.get(`${pantryApi}/integrations/xero/configurations/sync`, {})
    .then(response => { 
      if (response.data) {
        dispatch('fetchConfigurations')
        dispatch('fetchAccounts')
        dispatch('fetchContacts')
        commit('setApiResponse', {type: 'message', message: 'ok'})
        return true
      }
    })
    .catch(error =>  {
      commit('setApiResponse', error.response.data)
    })
  },

  fetchAccounts({ commit }) {
    if (!isLoggedin()) {
      return false
    }
    securedApi.get(`${pantryApi}/integrations/xero/accounts`, {})
    .then(response => { 
      if (response.data) {
        commit('setAccounts', response.data)
        commit('setApiResponse', {type: 'message', message: 'ok'})
      }
    })
    .catch(error =>  {
      commit('setApiResponse', error.response.data)
    })
  },
  
  fetchAllItems({ commit }) {
    if (!isLoggedin()) {
      return false
    }
    securedApi.get(`${pantryApi}/integrations/xero/items`, {})
    .then(response => { 
      if (response.data) {
        commit('setItems', response.data)
      }
    })
    .catch(error =>  {
      commit('setApiResponse', error.response.data)
    })
  },

  fetchContacts({ commit }, currentCompanyUuid) {
    return securedApi.get(`${pantryApi}/integrations/xero/contacts/${currentCompanyUuid}`, {})
    .then(response => { 
      if (response.data) {
        commit('setContacts', response.data)
        commit('setApiResponse', {type: 'message', message: 'ok'})
        return true
      }
    })
    .catch(error =>  {
      commit('setApiResponse', error.response.data)
      return true
    })
  },

  saveDefaults({ commit, dispatch }, { account }) {
    if (!isLoggedin()) {
      return false
    }
    securedApi.post(`${pantryApi}/integrations/xero/accounts/savedefaults`, { account })
    .then(response => {
      commit('setApiResponse', {type: 'message', message: 'ok'})
    })
    .catch(error =>  {
      commit('setApiResponse', error.response.data)
    })
  },

  saveContact({ commit, dispatch }, { contact } ) {
    if (!isLoggedin()) {
      return false
    }
    return securedApi.post(`${pantryApi}/integrations/xero/contacts/savecontact`, { contact })
    .then(response => {
      commit('setApiResponse', {type: 'message', message: 'ok'})
      return true
    })
    .catch(error =>  {
      commit('setApiResponse', error.response.data)
    })
  },

  saveCreditNoteContact({ commit, dispatch }, { contact } ) {
    if (!isLoggedin()) {
      return false
    }
    return securedApi.post(`${pantryApi}/integrations/xero/contacts/save_credit_note_contact`, { contact })
    .then(response => {
      commit('setApiResponse', {type: 'message', message: 'ok'})
      return true
    })
    .catch(error =>  {
      commit('setApiResponse', error.response.data)
    })
  },
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
