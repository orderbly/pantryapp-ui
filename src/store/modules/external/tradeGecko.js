import { securedAxiosInstance, plainAxiosInstance } from '../../../axios/index' 
import { pantryApi } from '@/constants/config'
import { getCookie } from '../../../constants/cookies'

let securedApi = securedAxiosInstance
const isLoggedin = function () {
return getCookie('access') !== undefined ? true : false
}

const state = {}

const getters = {}

const mutations = {}

const actions = {
  syncProducts({ commit, state }, companyId) {
    return securedApi.get(`${pantryApi}/integrations/tradegecko/products/retrieve_products/${companyId}`, {})
    .then(response => { 
      return response
    })
    .catch(err => err)
  },

  getContacts({ commit, state }, companyUuid) {
    return securedApi.get(`${pantryApi}/integrations/tradegecko/contacts/all_contacts/${companyUuid}`, {})
    .then(response => { 
      return response
    })
  },
  
  postSalesOrder({ commit, state }, {company_id, address_id, company_uuid, order_uuid}) {
    return securedApi.post(`${pantryApi}/integrations/tradegecko/orders/sales_order/${company_uuid}/${order_uuid}/${company_id}/${address_id}`)
    .then(response => { 
      if (response.data) {
        commit('orders/updateTradeGeckoSync', order_uuid, {root: true})
      }
      return response
    })
    .catch((err) => err)
  },

  createNewCompany({ commit, state }, {order_uuid, company_uuid}) {
    return securedApi.post(`${pantryApi}/integrations/tradegecko/contacts/create_company/${order_uuid}/${company_uuid}`)
    .then(response => { 
      return response
    })
    .catch((err) => err)
  },

  disconnect({ commit, state }, company_uuid) {
    securedApi.post(`${pantryApi}/integrations/tradegecko/disconnect/${company_uuid}`, {})
    .then(response => { 
      if (response.data) {
        return response
      }
    })
    .catch(error =>  {
      return error
    })
  },

  toggleOauthModal({ commit }) {
    commit('setOauthModal')
  }
}

export default {
namespaced: true,
state,
getters,
mutations,
actions
}
