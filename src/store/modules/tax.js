import { securedAxiosInstance, plainAxiosInstance } from '../../axios/index' 
import { pantryApi } from '@/constants/config'

let plainApi = plainAxiosInstance
let securedApi = securedAxiosInstance

const state = {
  taxes: [],
}

const getters = {
  getTaxes: state => state.taxes
}

const mutations = {
  setTaxes(state, data) {
    state.taxes = data
  },

  saveNewTag(state, tax) {
    let exisitingTax = state.taxes
    exisitingTax.push(tax)
    state.taxes = exisitingTax
  }
}

const actions = {

  fetchAllTaxes({ commit }) {
    return securedApi.get(`${pantryApi}/taxes/`, {}).then(r => {
      commit('setTaxes', r.data)
      return r
    })
  },

  postNewTax({ commit, dispatch }, tax) {
    securedApi.post(`${pantryApi}/taxes`, {
        tax
      }, {})
      .then(r => {
        commit('saveNewTag', r.data) 
        dispatch('modals/toggleNewTaxModal', null, { root: true })
      })
  },

  postEditTax({ commit, dispatch }, tax) {
    securedApi.patch(`${pantryApi}/taxes/${tax.id}/`, {
        tax
      }, {})
      .then(r => {
        dispatch('modals/toggleEditTaxModal', null, { root: true })
      })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
