import axios from 'axios'
import { securedAxiosInstance, plainAxiosInstance } from '../../axios/index' 
import { pantryApi, validateSageLoginUrl, sageApiKey } from '@/constants/config'
import { apiHandler } from '../../util/apiHandler';
import { getCookie, setCookie } from '../../constants/cookies'

let plainApi = plainAxiosInstance
let securedApi = securedAxiosInstance
const isLoggedin = function () {
  return getCookie('access') !== undefined ? true : false
}

const state = {
  integration_providers: [],
  order: {},
  displayIntegrationModal: false,
  displayAccountingModal: false,
}

const getters = {
  integrationProviders: state => state.integration_providers,
  accountOrder: state => state.order,
  getIntegrationModal: state => state.displayIntegrationModal,
  getAccountingModal: state => state.displayAccountingModal,
}

const mutations = {
  setIntegrationProviders(state, data) {
    state.integration_providers = data
  },
  setIntegrationModal(state) {
    state.displayIntegrationModal = !state.displayIntegrationModal
  },
  setAccountingModal(state) {
    state.displayAccountingModal = !state.displayAccountingModal
  },
  updateAccountingOrder(state, order) {
    state.order = order
  },
  clearAccountingOrder(state, order) {
    state.order = {}
  },
  toggle(state, {integration, status}) {
    const idx = state.integration_providers.findIndex((provider) => {
      return provider.integration == integration
    })
    state.integration_providers[idx].status = status
    state.integration_providers = state.integration_providers
  }

}

const actions = {
  fetchIntegrationProviders({ commit }) {
    return securedApi.get(`${pantryApi}/integrations/installed`, {})
    .then(response => { 
      if (response.data) {
        commit('setIntegrationProviders', response.data)
        return response
      }
    })
  },

  toggleIntegrationModal({ commit }) {
    commit('setIntegrationModal')
  },
  toggleAccountingModal({ commit }) {
    commit('setAccountingModal')
  },

  enableOptimo({ commit }, { apiKey, companyUuid }) {
    return securedApi.post(`${pantryApi}/integrations/optimo/${companyUuid}`, {
      api_key: apiKey
    }).then(() => {
      commit('toggle', { integration: 'optimo', status: 'Enabled'})
    })
  },
  
  disableOptimo({ commit }, { companyUuid }) {
    return securedApi.delete(`${pantryApi}/integrations/optimo/${companyUuid}`).then(() => {
      commit('toggle', { integration: 'optimo', status: 'Disabled'})
    })
  }

}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
