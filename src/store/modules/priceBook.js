import { securedAxiosInstance, plainAxiosInstance } from "../../axios/index";
import { pantryApi } from "../../constants/config";
import { apiHandler } from "../../util/apiHandler";

let plainApi = plainAxiosInstance;
let securedApi = securedAxiosInstance;

const state = {
  priceBooks: [],
  priceBookDiscounts: [],
  priceBookMemberships: [],
  companyList: []
};

const getters = {
  allPriceBooks: state => state.priceBooks,
  priceBookDiscounts: state => state.priceBookDiscounts,
  priceBookMemberships: state => state.priceBookMemberships,
  companyList: state => state.companyList
};

const mutations = {
  saveNewPriceBook(state, data) {
    state.priceBooks.push(data);
  },
  setCompanyList(state, data) {
    state.companyList = data;
  },
  setAllPriceBooks(state, data) {
    state.priceBooks = data.sort((a, b) =>
      a.title.toLowerCase() > b.title.toLowerCase() ? 1 : -1
    );
  },
  setAllPriceBookMemberships(state, data) {
    state.priceBookMemberships = data;
  },
  setProductVariantDiscount(state, data) {
    data.discount_list.forEach((pvd) => {
      let active_pvd = state.priceBookDiscounts.find((pbd) => {
        pbd.product_variant_id === pvd.product_variant_id
      })
      if (active_pvd != null) {
        state.priceBookDiscounts = state.priceBookDiscounts.filter((disc) => disc != active_pvd)
      }
      state.priceBookDiscounts.push(pvd)
    })
  },
  setPriceBookDiscounts(state, data) {
    state.priceBookDiscounts = data;
  },
  setDiscountBookMemberships(state, data) {
    data.forEach(mem => {
      state.priceBookMemberships.push(mem);
    });
  },
  setUpdateMemberships(state, price_book_id) {
    state.priceBookMemberships = state.priceBookMemberships.filter(
      pbm => pbm.id !== price_book_id
    );
  },
  removeDiscountBook(state, discountBookId) {
    state.priceBooks = state.priceBooks.filter(db => db.id !== discountBookId);
  }
};

const actions = {
  deleteMembership({ commit, state }, id) {
    let membership_id = id;
    securedApi
      .delete(`${pantryApi}/discount_book_memberships/${id}`, {})
      .then(r => {
        apiHandler(r);
        commit("setUpdateMemberships", membership_id);
      });
  },
  fetchPriceBookMemberships({ commit, state }, discount_book_id) {
    securedApi
      .get(`${pantryApi}/price_book_memberships/${discount_book_id}`, {})
      .then(r => {
        apiHandler(r);
        commit("setAllPriceBookMemberships", r.data);
      });
  },
  postNewPriceBook({ commit }, { title, description, supplier_id }) {
    const discount_book = {
      title: title,
      description: description,
      supplier_id: supplier_id
    };
    return securedApi
      .post(
        `${pantryApi}/discount_books`,
        {
          discount_book
        },
        {}
      )
      .then(res => {
        if (res.data.status) {
          commit("saveNewPriceBook", res.data.data);
        }
        return res;
      })
      .catch(err => {
        return err;
      });
  },
  fetchAllPriceBooks({ commit, state }) {
    securedApi
      .get(`${pantryApi}/discount_books`, {})
      .then(res => {
        commit("setAllPriceBooks", res.data);
      })
      .catch(error => apiHandler(error));
  },
  
  fetchPriceBookDiscounts({ commit, state }, { discount_book_id }) {
    securedApi
      .get(`${pantryApi}/price_book_discounts/${discount_book_id}`, {})
      .then(res => {
        commit("setPriceBookDiscounts", res.data);
      })
      .catch(error => apiHandler(error));
  },
  deletePriceBook({ commit, state }, discountBookId) {
    return securedApi
      .delete(`${pantryApi}/discount_books/${discountBookId}`, {})
      .then(res => {
        if (res.data.status === true) {
          commit("removeDiscountBook", discountBookId);
          return res;
        }
        return res;
      })
      .catch(err => {
        return err;
      });
  },
  getPriceBookCompanies({ commit }, discount_book_id) {
    securedApi
      .get(`${pantryApi}/companies/list_all_companies/${discount_book_id}`, {})
      .then(res => {
        commit("setCompanyList", res.data);
      })
      .catch(error => apiHandler(error));
  },
  newProductVariantDiscounts({ commit, state }, product_variant_discounts) {
    return securedApi
      .post(
        `${pantryApi}/product_variant_discounts`,
        product_variant_discounts,
        {}
      )
      .then(res => {
          commit("setPriceBookDiscounts", res.data.product_variant_discounts);
          return res.data.all_added;
      })
      .catch((error) => {return error;});
  },
  editProductVariantDiscount({ commit, state }, product_variant_discount) {
    securedApi
      .patch(
        `${pantryApi}/product_variant_discounts/${product_variant_discount.id}`,
        product_variant_discount,
        {}
      )
      .then(res => {
        commit("setProductVariantDiscount", res.data);
      })
      .catch(error => apiHandler(error));
  },
  addNewPriceBookMembership({ commit, state }, membership_payload) {
    let newStateMemberships = [];
    return securedApi
      .post(`${pantryApi}/discount_book_memberships`, membership_payload, {})
      .then(res => {
        commit("setDiscountBookMemberships", res.data.memberships);
        return res
      })
      .catch(error => apiHandler(error));
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
