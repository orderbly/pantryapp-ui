import { securedAxiosInstance, plainAxiosInstance } from '../../axios/index' 
import { pantryApi } from '../../constants/config'
import { apiHandler } from '../../util/apiHandler';

let plainApi = plainAxiosInstance
let securedApi = securedAxiosInstance

const state = {
  products: [],
  items: null,
  activeProduct: null,
  variants: null
}

const getters = {
  getProducts: state => state.products.sort((a, b) => a.id - b.id),
  getActiveProduct: state => state.activeProduct,
  getAllSupplierProducts: state => state.items,
  getProductVariants: state => state.variants
}

const mutations = {
  updateProductVariantStock(state, cartItem) {
    state.items.forEach((item) => {
      item.product_variants.forEach(pv => {
        if (pv.id == cartItem.product_variant_id) {
          pv.available_stock += pv.original_stock
        }
      })
    })
  },

  removeProduct(state, id) {
    state.products = state.products.filter(p => p.id != id)
  },
  setSupplierProducts(state, products) {
    state.items = []
    state.items = products
    state.items.forEach((item) => {
      item.product_variants.map(pv => {
        pv.original_stock = pv.available_stock
      })
    })
  },
  setProductList(state, products) {
    state.products = products
  },
  setProductVariants(state, variants) {
    state.variants = variants
  },
  setProductError(state, error) {
    state.error = error
  },
  setActiveProduct(state, product) {
    state.activeProduct = null
    state.activeProduct = product
  },
  setNewProduct(state, product) {
    state.products.push(product)
  },
  setBaseQuantity (state, props)  {
    state.items.forEach((item) => {
      if (props.product_id === item.product.id) {
        item.product_variants.map(pv => {
          if (pv.id === props.product_variant_id) {
            pv.available_stock = pv.stock
          }
        })
      }
    })
  },

  updateProductVariantQuantity(state, props) {
    state.items.forEach((item) => {
      if (props.product_id === item.product.id) {
        item.product_variants.map(pv => {
          if (pv.id === props.product_variant_id) {
            if (pv.stock <= 0) pv.stock = pv.available_stock
            if(parseInt(pv.stock) - parseInt(props.quantity) > 0) {
              let holdingStock = (parseInt(pv.stock) - parseInt(pv.available_stock)) + parseInt(props.quantity)
              pv.available_stock = parseInt(pv.stock) - holdingStock
            } else {
              pv.available_stock = 0
            }
          }
        })
      }
    })
  },
  updateQuickSelectProductVariantQuantity(state, props) {
    state.items.forEach((item) => {
      if (props.product_id === item.product.id) {
        item.product_variants.map(pv => {
          if (pv.id === props.product_variant_id) {
            if (pv.stock <= 0) pv.stock = pv.available_stock
            pv.available_stock = parseInt(pv.stock) - props.quantity
          }
        })
      }
    })
  },
  updateProductCartVariantQuantity(state, product) {
    state.items.forEach((item) => {
      if (product.product_id === item.product.id) {
        item.product_variants.map(pv => {
          if (pv.id === product.product_variant_id) {
            if (pv.stock <= 0) pv.stock = pv.available_stock
            if(parseInt(pv.stock) - parseInt(product.quantity) > 0) {
              pv.available_stock = parseInt(pv.stock) - parseInt(product.quantity)
            } else {
              pv.available_stock = 0
            }
          }
        })
      }
    })
  },
  updateProduct(state, product) {
    let products = state.products.filter(p => p.id !== product.id)
    products.push(product)
    state.products = products
  }
}

const actions = {
  updateQuantityFromCart  ({ commit }, product) {
    commit('updateProductCartVariantQuantity', product)
  },

  resetQuantity  ({ commit }, props) {
    commit('setBaseQuantity', props)
  },

  updateQuantity ({ commit }, props) {
    commit('updateProductVariantQuantity', props)
  },

  fetchVariantsByProduct({ commit }, id) {
    securedApi.get(`${pantryApi}/variants_by_product/${id}`, {}).then(r => {
      commit('setProductVariants', r.data)
    })
    .catch(error =>
      apiHandler(error)
    )
  },

  toggleActiveProduct({ commit }, id) {
    securedApi.post(`${pantryApi}/toggle_active_product/${id}`, {}).then(r => {
      return true
    })
    .catch(() => {
      return false
    })
  },

  fetchAllProducts({ commit }, company_id) {
    return securedApi.get(`${pantryApi}/get_company_products/${company_id}`, {})
      .then(r => {
        commit('setProductList', r.data)
        return true
      })
      .catch(error => {
        return true
      })
  },

  removeProductVariant({ commit }, id) {
    securedApi.patch(`${pantryApi}/product_variants/disable_product_variant/${id}`, {})
  },

  postNewProduct({ commit }, {product}) {
    return securedApi.post(`${pantryApi}/products`, product)
    .then(function(response) {
        commit('setNewProduct', response.data)
        return response
    }).catch(error => error)
  },
  // postProductTags({ commit }, {product_variant_id, tags, type}) {
  //   securedApi.put(`${pantryApi}/product_add_tag/${product_variant_id}`, {
  //         product: {
  //           id: product_variant_id,
  //           tags: tags
  //         },
  //       }, {})
  //     .then(function(response) {
  //       if (type === 'new') {
  //         commit('setNewProduct', response.data)
  //       } else {
  //         commit('updateProduct', response.data)
  //       }
  //     })
  //     .catch(error =>
  //       apiHandler(error)
  //     )
  // },
  deleteProduct({ commit }, id) {
    securedApi.delete(`${pantryApi}/products/${id}`, {})
    .then(res => {
      commit('removeProduct', id)
    })
    .catch(error =>
      apiHandler(error)
    )
  },
  fetchSupplierProducts({ commit }, id) {
    return securedApi.get(`${pantryApi}/company_products/${id}`, {})
    .then(res => {
      commit('setSupplierProducts', res.data)
      return res
    })
    .catch((error) => {
      return error
    })
  },

  fetchSupplierProductsByOrderPage({ commit }, {title, customerId}) {
    return securedApi.get(`${pantryApi}/company_products_for_order/${title}/${customerId}`, {})
    .then(res => {
      commit('setSupplierProducts', res.data)
      return res
    })
    .catch((error) => {
      return error
    })
  },

  selectActiveProduct({ commit, state }, id) {
    let activeProduct = state.items.filter(p => p.id === id)
    commit('setActiveProduct', activeProduct[0])
  },
  postEditProduct({ commit, dispatch }, {id, product, tags, type}) {
    return securedApi
      .patch(`${pantryApi}/products/${id}`, product)
      .then(function(response) {
        if(tags.length > 0){
          dispatch('postProductTags', 
            {
              product_variant_id: response.data.id, 
              tags: tags,
              type: type
            }
          )
          return response
        } else {
          commit('updateProduct', response.data)
          return response
        }
      }).catch(error => error)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}

