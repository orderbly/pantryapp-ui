import { securedAxiosInstance, plainAxiosInstance } from '../../axios/index' 
import { pantryApi } from '@/constants/config'
import { apiHandler } from '../../util/apiHandler';

let plainApi = plainAxiosInstance
let securedApi = securedAxiosInstance

const state = {
  tags: [],
}

const getters = {
  getAllTags: state => state.tags
}

const mutations = {
  setSupplierList(state, items) {
    state.suppliers = items
  },

  setTags(state, data) {
    state.tags = data
  },

  saveNewTag(state, tag) {
    state.tags.push(tag)
  }
}

const actions = {

  fetchAllTags({ commit }) {
    securedApi.get(`${pantryApi}/tags/`, {}).then(r => {
      apiHandler(r)
      commit('setTags', r.data)
    })
  },
  
  removeProductTag({ commit }, params) {
    let payload = {
      product: {
          id: params.productId,
          tag_id: params.tag_id
        }
    }
    securedApi.delete(`${pantryApi}/product_delete_tag/${params.productId}/${params.tag_id}/`, {
        payload
    }, {}).then(r => {
      apiHandler(r)
    })
  },

  postNewTag({ commit, state }, tagName) {
    const tag = { description: tagName}
    return securedApi.post(`${pantryApi}/tags/`, { tag })
      .then(r => {
        commit('saveNewTag', r.data) 
        return r.data
      })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
