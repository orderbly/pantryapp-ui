import { securedAxiosInstance } from '../../axios/index' 
import { pantryApi } from '@/constants/config'
import { getCookie } from '../../constants/cookies'

let securedApi = securedAxiosInstance
const isLoggedin = function () {
  return getCookie('access') !== undefined ? true : false
}

const state = {
  tradeStatement: {},
  customersIndexStatement: [],
  suppliersIndexStatement: [],
}

const getters = {
  suppliersIndexStatement: state => state.suppliersIndexStatement,
  customersIndexStatement: state => state.customersIndexStatement,
  supplierAnalysis: state => state.supplierAnalysis,
  tradeStatement: state => state.tradeStatement
}

const mutations = {
  setCustomersIndexStatement(state, data) {
    state.customersIndexStatement = data
  },
  setSuppliersIndexStatement(state, data) {
    state.suppliersIndexStatement = data
  },
  setTradeStatement(state, data) {
    state.tradeStatement = data
  },
  clearTradeStatements (state) {
    state.tradeStatement = []
  }
}

const actions = {
  fetchCustomersIndexStatement({ commit }, supplier_uuid) {
    securedApi.get(`${pantryApi}/statements/customers_index_statement/${supplier_uuid}`, {})
    .then(response => { 
      if (response.data) {
        commit('setCustomersIndexStatement', response.data)
      }
    }).catch(err => err)
  },

  fetchSupplierIndexStatement({ commit }, customer_uuid) {
    return securedApi.get(`${pantryApi}/statements/suppliers_index_statement/${customer_uuid}`, {})
    .then(response => { 
      if (response.data) {
        commit('setSuppliersIndexStatement', response.data)
        return response
      }
    }).catch(err => err)
  },

  fetchTradeStatement({ commit }, {supplier_uuid, customer_uuid, startDate, endDate}) {
    return securedApi.get(`${pantryApi}/statements/trade_statement/${supplier_uuid}/${customer_uuid}/${startDate}/${endDate}`, {})
    .then(response => { 
      if (response.data) {
        commit('setTradeStatement', response.data)
        return response
      }
    }).catch(err => err)
  },
  
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
