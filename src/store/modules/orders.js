import { securedAxiosInstance, plainAxiosInstance } from '../../axios/index' 
import { pantryApi } from '../../constants/config'
import { apiHandler, async } from '../../util/apiHandler';
import DashboardPlugin from '../../plugins/dashboard-plugin'
import Vue from 'vue'

let plainApi = plainAxiosInstance
let securedApi = securedAxiosInstance

const filterStartDate = function () {
  let date = new Date();
  let y = date.getFullYear();
  let m = date.getMonth();
  return new Date(y, 0, 1);
}

const filterEndDate = function () {
  let date = new Date();
  let y = date.getFullYear();
  let m = date.getMonth();
  return new Date(y, m + 1, 0);
}

const state = {
  current_credit_note: {},
  activeOrder: {},
  purchaseClientFilter: {
    isFiltering: false,
    clientName: "All Suppliers",
    startDate: filterStartDate(),
    endDate: filterEndDate()
  },
  saleOrderFilter: {
    isFiltering: false,
    clientName: "All Customers",
    startDate: filterStartDate(),
    endDate: filterEndDate()
  },
  saleClientFilter: null,
  salesOrders: [],
  pendingSalesOrders: [],
  declinedOrders: [],
  acceptedSalesOrders: [],
  orders: [],
  calendarOrders: [],
  cancelledPurchaseOrders: [],
  pendingPurchaseOrders: [],
  declinedPurchaseOrders: [],
  acceptedPurchaseOrders: [],
  credit_notes_received: [],
  currentOrder: null,
  notifyEditSuccess: false,
  notifyDeleteSuccess: false,
}

const getters = {
  // sales
  getCurrentCreditNote: state => state.current_credit_note,

  creditNotesReceived: state => state.credit_notes_received,

  activeOrder: state => state.activeOrder,

  totalSales (state) {
    let sales = 0
    state.salesOrders.forEach(o => sales += o.gross_amount)
    return sales
  },

  numberOfSales (state) {
    return state.salesOrders.length
  },

  averageOrderValue (state, getters) {
    return typeof (getters.totalSales / state.salesOrders.length) === "number" ? getters.totalSales / state.salesOrders.length : 0
  },

  calendarOrders (state) {
    return state.calendarOrders
  },

  saleFilterDates (state) {
    return {startDate: state.saleOrderFilter.startDate, endDate: state.saleOrderFilter.endDate}
  },

  saleFilterClient (state) {
    return state.saleOrderFilter.clientName
  },

  isFilterSales (state) {
    return state.saleOrderFilter.isFiltering
  },

  allSalesOrders (state) {
    if (state.saleOrderFilter.clientName && state.saleOrderFilter.clientName !== "All Customers") {
      return state.salesOrders.filter(o => o.customer === state.saleOrderFilter.clientName)
    } else {
      return state.salesOrders
    }
  },
  allPendingSalesOrders (state) {
    if (isFilteringClient(state.saleOrderFilter.clientName)) {
      return saleStatusAndClientFilter(state.salesOrders, 'pending', state.saleOrderFilter.clientName)
    } else {
      return statusFilter(state.salesOrders, 'pending')
    }
  },

  allDeclinedSalesOrders (state) {
    if (isFilteringClient(state.saleOrderFilter.clientName)) {
      return saleStatusAndClientFilter(state.salesOrders, 'declined', state.saleOrderFilter.clientName)
    } else {
      return statusFilter(state.salesOrders, 'declined')
    }
  },
  
  allAcceptedSalesOrders (state) {
    if (isFilteringClient(state.saleOrderFilter.clientName)) {
      return saleStatusAndClientFilter(state.salesOrders, 'accepted', state.saleOrderFilter.clientName)
    } else {
      return statusFilter(state.salesOrders, 'accepted')
    }
  },

  allCancelledSalesOrders (state) {
    if (isFilteringClient(state.saleOrderFilter.clientName)) {
      return saleStatusAndClientFilter(state.salesOrders, 'cancelled', state.saleOrderFilter.clientName)
    } else {
      return statusFilter(state.salesOrders, 'cancelled')
    }
  },
  
  
  // purchases

  isPurchaseFiltering (state) {
    return isFilteringClient(state.purchaseClientFilter.isFiltering)
  },

  totalPurchases (state, getters) {
    let purchases = 0
    getters.allOrders.forEach(o => purchases += o.gross_amount)
    return purchases
  },

  averagePurchaseOrderValue (state, getters) {
    return getters.totalPurchases / getters.allOrders.length
  },

  numberOfPurchases (state) {
    return state.orders.length
  },

  filterDates (state) {
    return {startDate: state.purchaseClientFilter.startDate, endDate: state.purchaseClientFilter.endDate}
  },

  filterClient (state) {
    return state.purchaseClientFilter.clientName
  },

  allOrders (state) {
    let clientName = state.purchaseClientFilter.clientName
    if (clientName && clientName !== "All Suppliers") {
      return state.orders.filter(o => o.supplier.title === clientName)
    } else {
      return state.orders
    }
  },

  allAcceptedPurchaseOrders(state)  {
    if (isFilteringClient(state.purchaseClientFilter.clientName)) {
      return statusAndClientFilter(state.orders, 'accepted', state.purchaseClientFilter.clientName)
    } else {
      return statusFilter(state.orders, 'accepted')
    }
  },

  allCancelledPurchaseOrders(state)  {
    if (isFilteringClient(state.purchaseClientFilter.clientName)) {
      return statusAndClientFilter(state.orders, 'cancelled', state.purchaseClientFilter.clientName)
    } else {
      return statusFilter(state.orders, 'cancelled')
    }
  },

  allPendingPurchaseOrders(state)  {
    if (isFilteringClient(state.purchaseClientFilter.clientName)) {
      return statusAndClientFilter(state.orders, 'pending', state.purchaseClientFilter.clientName)
    } else {
      return statusFilter(state.orders, 'pending')
    }
  },

  allDeclinedPurchaseOrders(state)  {
    if (isFilteringClient(state.purchaseClientFilter.clientName)) {
      return statusAndClientFilter(state.orders, 'declined', state.purchaseClientFilter.clientName)
    } else {
      return statusFilter(state.orders, 'declined')
    }
  },

  notifyEditSuccess: state => state.notifyEditSuccess,
  notifyDeleteSuccess: state => state.notifyDeleteSuccess,

  getCurrentOrder: state => state.currentOrder,

  orderSuppliers(state) {
    let suppliers = ["All Suppliers"]
    if (state.orders.length > 0) {
      state.orders.map(o => {
        if (!suppliers.includes(o.supplier.title)) {
          suppliers.push(o.supplier.title)
        }
      })
    }
    return suppliers
  },
  
  orderCustomers(state) {
    let suppliers = ["All Customers"]
    if (state.salesOrders.length > 0) {
      state.salesOrders.map(o => {
        if (!suppliers.includes(o.customer.title)) {
          suppliers.push(o.customer.title)
        }
      })
    }
    return suppliers
  }
}

const mutations = {

  updateTradeGeckoSync (state, order_uuid) {
    state.salesOrders = state.salesOrders.map(o => {
      if (o.order_uuid === order_uuid) {
        o.integration_inventory = true
      }
      return o
    })
  },

  updateOptimoSync (state, order_uuid) {
    state.salesOrders = state.salesOrders.map(o => {
      if (o.order_uuid === order_uuid) {
        o.integration_delivery = true
      }
      return o
    })
  },

  updateIntegratedOrder(state, order) {
    if (state.orders.length > 0) {
      state.orders = state.orders.map(o => {
        if (o.order_number === order.order_number) {
          o.integration_purchases = true
        }
        return o
      })
    }
  },

  setCurrentCreditNote (state, credit_note) {
    state.current_credit_note = credit_note
  },

  updateEditOrderNotify(bool) {
    state.notifyEditSuccess = bool
    setTimeout(() => {
      state.notifyEditSuccess = false
    }, 200)
  },
  
  updateDeleteOrderNotify(bool) {
    state.notifyDeleteSuccess = bool
    setTimeout(() => {
      state.notifyDeleteSuccess = false
    }, 200)
  },

  updatePurchaseOrderFilter(state, {startDate, endDate, clientName}) {
    state.purchaseClientFilter = {
      isFiltering: true,
      clientName: clientName,
      startDate: startDate,
      endDate: endDate
    }
  },

  updateSaleOrderFilter(state, {startDate, endDate, clientName}) {
    state.saleOrderFilter = {
      isFiltering: true,
      clientName: clientName,
      startDate: startDate,
      endDate: endDate
    }
  },

  updateSaleOrderClientFilter(state, clientName) {
    state.saleOrderFilter.clientName = clientName
  },

  setActiveOrder(state, order) {
    state.activeOrder = order
  },

  setCreditNotesReceived(state, credit_notes) {
    state.credit_notes_received = credit_notes
  },

  updateClientFilter(state, data) {
    state.purchaseClientFilter = data
  },

  updateSaleClientFilter(state, data) {
    state.saleClientFilter = data
  },

  setOrderFrequency(state, data) {
    state.currentOrder.isRecurring = true
    state.current_order = data
  },

  setAllPurchaseOrders(state, data) {
    state.orders = data
  },

  setCalendarOrders(state, data) {
    state.calendarOrders = data
  },

  updateStatus(state, data) {
    state.currentOrder = data
  },

  setSalesOrders(state, data) {
    state.salesOrders = data
  },

  setPendingSalesOrders(state, data) {
    state.pendingSalesOrders = data
  },

  setAcceptedSalesOrders(state, data) {
    state.acceptedSalesOrders = data
  },

  setCurrentOrder(state, order) {
    state.currentOrder = order
  },

  setDeclinedSalesOrders(state, order) {
    state.declinedOrders = order
  },

  setCancelledPurchaseOrders(state, order) {
    state.cancelledPurchaseOrders = order
  },

  setDeclinedPurchaseOrders(state, order) {
    state.declinedPurchaseOrders = order
  },

  setAcceptedPurchaseOrders(state, order) {
    state.acceptedPurchaseOrders = order
  },

  setPendingPurchaseOrders(state, order) {
    state.pendingPurchaseOrders = order
  },

  updateCurrentOrderComments(state, comment) {
  if (state.currentOrder.order_comments.filter(c => c.id === comment.id).length === 0)
    state.currentOrder.order_comments.push(comment)
  },

  setIncomingOrder(state, order) {
    state.salesOrders.unshift(order)
    if (state.pendingSalesOrders.length !== 0) {
      state.pendingSalesOrders.unshift(order)
    }
  },
  updateCancelledPurchaseOrders(state, order) {
    if (state.cancelledPurchaseOrders.length > 0) {
      state.cancelledPurchaseOrders.unshift(order)
    }
    if (state.salesOrders.length > 0) {
      state.salesOrders.map(o => {
        if (o.order_number === order.order_number) {
          o.order_status = order.order_status
        }
      })
    }
  },

  updateDeclinedPurchaseOrders(state, order) {
    if (state.pendingPurchaseOrders.length > 0) {
      state.pendingPurchaseOrders = state.pendingPurchaseOrders.filter(o => o.order_number !== order.order_number)
    }
    if (state.declinedPurchaseOrders.length > 0) {
      state.declinedPurchaseOrders.unshift(order)
    }
    if (state.orders.length > 0) {
      state.orders.map(o => {
        if (o.order_number === order.order_number) {
          o.order_status = order.order_status
        }
      })
    }
  },

  updateAcceptedPurchaseOrders(state, order) {
    if (state.pendingPurchaseOrders.length > 0) {
      state.pendingPurchaseOrders = state.pendingPurchaseOrders.filter(o => o.order_number !== order.order_number)
    }
    if (state.acceptedPurchaseOrders.length > 0) {
      state.acceptedPurchaseOrders.unshift(order)
    }
    if (state.orders.length > 0) {
      state.orders.map(o => {
        if (o.order_number === order.order_number) {
          o.order_status = order.order_status
        }
      })
    }
  },

  updateAcceptedSalesOrders(state, order) {
    if (state.pendingSalesOrders.length > 0) {
      state.pendingSalesOrders = state.pendingSalesOrders.filter(o => o.order_number !== order.order_number)
    }
    if (state.acceptedSalesOrders.length > 0) {
      state.acceptedSalesOrders.unshift(order)
    }
    if (state.salesOrders.length > 0) {
      state.salesOrders.map(o => {
        if (o.order_number === order.order_number) {
          o.order_status = order.order_status
        }
      })
    }
  },

  updateDeclinedSalesOrders(state, order) {
    if (state.pendingSalesOrders.length > 0) {
      state.pendingSalesOrders = state.pendingSalesOrders.filter(o => o.order_number !== order.order_number)
    }
    if (state.declinedOrders.length > 0) {
      state.declinedOrders.unshift(order)
    }
    if (state.salesOrders.length > 0) {
      state.salesOrders.map(o => {
        if (o.order_number === order.order_number) {
          o.order_status = order.order_status
        }
      })
    }
  }
}

const actions = {

  updateDeliveryRecon({ commit }, order_uuid) {
    return securedApi.post(`${pantryApi}/update_delivery_reconcilation/${order_uuid}`)
    .then((res) => {
      commit("setCurrentOrder", res.data)
      return res
    })
    .catch(error => error)
  },

  updateDeliverySignature({ commit }, order_uuid) {
    return securedApi.post(`${pantryApi}/update_order_delivery_signature/${order_uuid}`)
    .then((res) => {
      commit("setCurrentOrder", res.data)
      return res
    })
    .catch(error => error)
  },

  updateCancelledOrder({ commit }, order) {
    commit('updateCancelledPurchaseOrders', order)
  },

  updateDeclinedOrder({ commit }, order) {
    commit('updateDeclinedPurchaseOrders', order)
  },

  updateAcceptedOrder({ commit }, order) {
    commit('updateAcceptedPurchaseOrders', order)
  },

  addLiveOrderComment({ commit }, comment) {
    commit('updateCurrentOrderComments', comment)
  },

  saveIncomingOrder({ commit, state }, data) {
    commit('setIncomingOrder', data)
  },

  updateDeliveryFee({ commit }, {order_uuid, data}) {
    return securedApi.post(`${pantryApi}/update_delivery_fee/${order_uuid}`, data)
    .then((res) => {
      commit("setCurrentOrder", res.data)
      return res
    })
    .catch(error => error)
  },

  async updateDeliveryAddress({ commit }, deliverAddress) {
    return securedApi.patch(`${pantryApi}/orders/${deliverAddress.id}`, deliverAddress).then(res => {
      if (res.status) {
        commit('setCurrentOrder', res.data)
        return true
      }
    }).catch(() => {
      return false
    })
  },

  async fetchAllOrders({ commit, state }) {
    return securedApi.get(`${pantryApi}/summary`, {}).then(res => {
      if (res.status) {
        commit('setAllPurchaseOrders', res.data)
        return true
      }
    }).catch(() => {
      return false
    })
  },

  acceptOrder({ commit, state }, id) {
    securedApi.get(`${pantryApi}/accept_order/${id}`, {}).then(res => {
      if (res.status) {
        state.salesOrders.forEach(m => {
          if (m.order_number === id) {
            m.order_status = "accepted"
          }
        })
        commit('updateAcceptedSalesOrders', res.data)
        commit('updateStatus', res.data)
      }
    })
  },

  declineOrder({ commit, state }, id) {
    securedApi.get(`${pantryApi}/decline_order/${id}`, {}).then(res => {
      if (res.status) {
        state.salesOrders.forEach(m => {
          if (m.order_number === id) {
            m.order_status = "declined"
          }
        })
        commit('updateDeclinedSalesOrders', res.data)
        commit('updateStatus', res.data)
      }
    })
  },

  completeOrder({ commit, state }, id) {
    state.orders.forEach(m => {
      if (m.order_number === id) {
        m.order_status = "completed"
      }
    })
  },

  editOrder({ commit, state }, {order_id, values}) {
    return securedApi.put(`${pantryApi}/edit_order/${order_id}`, values).then(res => {
      if (res.status) {
        commit('setCurrentOrder', res.data.obj)
        commit('updateEditOrderNotify', true)
        return res
      }
    }).catch((err) => {
      return err
    })
  },

  deleteOrderItem({ commit, state }, id) {
    securedApi.delete(`${pantryApi}/order_items/${id}`, {}).then(res => {
      if (res) {
        commit('setAllPurchaseOrders', res.data)
        commit('updateDeleteOrderNotify', true)
      }
    })
  },

  cancelOrder({ commit, state }, id) {
    securedApi.get(`${pantryApi}/cancel_order/${id}`, {}).then(res => {
      if (res.status) {
        state.orders.forEach(m => {
          if (m.order_number === id) {
            m.order_status = "cancelled"
          }
        })
        commit('updateStatus', res.data)
      }
    })
  },

  fetchSalesOrders({ commit, state }) {
    let order_length = state.salesOrders.length
    return securedApi.get(`${pantryApi}/sales_order_summary`, {})
    .then(res => {
      if (res.status) {
        commit('setSalesOrders', res.data)
        return true
      }
    })
    .catch(() => {
      return false
    })
  },

  fetchPendingSalesOrders({ commit, state }) {
    securedApi.get(`${pantryApi}/sales_summary/pending`, {}).then(res => {
      if (res.status) {
        commit('setPendingSalesOrders', res.data)
      }
    })
  },

  fetchCalendarOrders({ commit, state }) {
    securedApi.get(`${pantryApi}/calendar_summary`, {}).then(res => {
      if (res.status) {
        commit('setCalendarOrders', res.data)
      }
    })
  },

  fetchAcceptedSalesOrders({ commit, state }) {
    securedApi.get(`${pantryApi}/sales_summary/accepted`, {}).then(res => {
      if (res.status) {
        commit('setAcceptedSalesOrders', res.data)
      }
    })
  },

  saveActiveOrder({ commit }, order) {
    commit('setActiveOrder', order)
  },

  fetchDeclinedSalesOrders({ commit, state }) {
    securedApi.get(`${pantryApi}/sales_summary/declined`, {}).then(res => {
      if (res.status) {
        commit('setDeclinedSalesOrders', res.data)
      }
    })
  },

  fetchPendingPurchaseOrders({ commit, state }) {
    securedApi.get(`${pantryApi}/purchase_order_summary/pending`, {}).then(res => {
      if (res.status) {
        commit('setPendingPurchaseOrders', res.data)
      }
    })
  },

  fetchAcceptedPurchaseOrders({ commit, state }) {
    securedApi.get(`${pantryApi}/purchase_order_summary/accepted`, {}).then(res => {
      if (res.status) {
        commit('setAcceptedPurchaseOrders', res.data)
      }
    })
  },

  fetchDeclinedPurchaseOrders({ commit, state }) {
    securedApi.get(`${pantryApi}/purchase_order_summary/declined`, {}).then(res => {
      if (res.status) {
        commit('setDeclinedPurchaseOrders', res.data)
      }
    })
  },

  fetchCurrentCreditNote({ commit, state }, id) {
    return securedApi.get(`${pantryApi}/credit_notes/current_credit_note/${id}`, {}).then(res => {
      if (res.status) {
        commit('setCurrentCreditNote', res.data)
        return true
      }
    }).catch(() => {
      return false
    })
  },

  fetchCancelledPurchaseOrders({ commit, state }) {
    securedApi.get(`${pantryApi}/purchase_order_summary/cancelled`, {}).then(res => {
      if (res.status) {
        commit('setCancelledPurchaseOrders', res.data)
      }
    })
  },

  fetchOrder({ commit, state }, id) {
    return securedApi.get(`${pantryApi}/order_breakdown/${id}`, {})
    .then(res => {
      if (res.status) {
        commit('setCurrentOrder', res.data)
        return true
      }
    })
    .catch(() => {
      return false
    })
  },

  fetchOrderDetail({commit, state}, {current_company, order_uuids}) {
    return securedApi.post(`${pantryApi}/order_items_breakdown/${current_company.uuid}`, {order_uuids})
    .then(res => {
      if (res.status) {
        return res.data
      }
    })
    .catch(() => {
      return false
    })
  },

  addOrderComments({ commit, state }, comment) {
    securedApi.post(`${pantryApi}/comments`, { 
      comment
    }, {})
    .then(function(res) {
      commit("updateCurrentOrderComments", res.data)
    })
    .catch(error =>
      apiHandler(error)
    )
  },

  filterSaleOrders({ commit, state }, {start_date, end_date, clientName}) {
    return securedApi.get(`${pantryApi}/filter_sales_orders/${start_date}/${end_date}`, {})
      .then(function(res) {
        commit("setSalesOrders", res.data)
        commit("updateSaleOrderFilter", {startDate: start_date, endDate: end_date, clientName: clientName})
        return true
      })
      .catch(error => {
        return false
      })
  },

  resetSaleOrders({ commit, state }) {
    let start_date = filterStartDate()
    let end_date = filterEndDate()
    if (start_date.toString().substring(0,11) !== state.saleOrderFilter.startDate.toString().substring(0,11) || end_date.toString().substring(0,11) !== state.saleOrderFilter.endDate.toString().substring(0,11)) {
      securedApi.get(`${pantryApi}/filter_sales_orders/${start_date}/${end_date}`, {})
      .then(function(res) {
        commit("setSalesOrders", res.data)
        commit("updateSaleOrderFilter", {startDate: start_date, endDate: end_date, clientName: "All Customers"})
      })
      .catch(error =>
        apiHandler(error)
      )
    }
  },

  resetPurchaseOrders({ commit, state }) {
    let start_date = filterStartDate()
    let end_date = filterEndDate()
    securedApi.get(`${pantryApi}/filter_purchase_orders/${start_date}/${end_date}`, {})
    .then(function(res) {
      commit("setAllPurchaseOrders", res.data)
      commit("updateSaleOrderFilter", {startDate: start_date, endDate: end_date, clientName: "All Suppliers"})
    })
    .catch(error =>
      apiHandler(error)
    )
  },


  updateOrderDeliveryStatus({ commit }, { id, status }) {
    return securedApi
      .post(`${pantryApi}/update_single_order_delivery_status/${id}`, {
        delivery_status: status
      })
      .then(res => {
        commit("setCurrentOrder", res.data);
        return true;
      })
      .catch(() => {
        return false;
      });
  },

  async postDeliveryOrderUser({ commit }, delivery_allocation) {
    return securedApi.post(`${pantryApi}/delivery_allocations`, {
      delivery_allocation
    })
    .then((res) => {
      commit('setCurrentOrder', res.data)
      return res
    })
    .catch(error =>
      console.log(error)
    )
  },

  async setFrequency({ commit }, payload) {
    return securedApi.post(`${pantryApi}/create_order_schedule`, payload)
    .then(function(res) {
      commit('setCurrentOrder', res.data)
      return res
    })
    .catch(error =>
      console.log(error)
    )
  },

  async deleteScheduledOrder({ commit }, id) {
    return securedApi.post(`${pantryApi}/delete_scheduled_order/${id}`, {})
    .then(function(res) {
      commit('setCurrentOrder', res.data)
      return true
    })
    .catch(() => {
      return false
    })
  },

  async deleteDeliveryAssignee({ commit }, delivery_allocation) {
    return securedApi.post(`${pantryApi}/delete_delivery_allocation`, delivery_allocation)
    .then(function(res) {
      commit('setCurrentOrder', res.data)
      return true
    })
    .catch(() => {
      return false
    })
  },

  async creditNotesReceived({ commit }) {
    return securedApi.get(`${pantryApi}/credit_notes/received_credit_notes`, {})
    .then(function(res) {
      commit('setCreditNotesReceived', res.data)
      return true
    })
    .catch(() => {
      return false
    })
  },

  async claimGeneralCredit({ commit }, creditNote) {
    return securedApi.post(`${pantryApi}/credit_notes/general_credit_note`, creditNote)
    .then(function(res) {
      return res
    })
    .catch((err) => {
      return err
    })
  },

  async duplicateOrder({ commit }, id) {
    return securedApi.post(`${pantryApi}/duplicate_order/${id}`)
    .then(function(res) {
      return res
    })
    .catch((err) => {
      return err
    })
  },

  async payOrderDirectly({ commit }, id) {
    return securedApi.post(`${pantryApi}/payments/pay_order_directly/${id}`)
    .then(function(res) {
      commit('setCurrentOrder', res.data)
      return true
    })
    .catch((err) => {
      return false
    })
  },

  async addItemToOrder({ commit }, payload) {
    return securedApi.post(`${pantryApi}/add_item_to_order`, payload)
    .then(function(res) {
      commit('setCurrentOrder', res.data)
      localStorage["backupItems"]= JSON.stringify(res.data.order_items)
      return true
    })
    .catch((err) => {
      return false
    })
  },

  filterPurchaseOrders({ commit, state }, {start_date, end_date, clientName}) {
    return securedApi.get(`${pantryApi}/filter_purchase_orders/${start_date}/${end_date}`, {})
    .then(function(res) {
      commit("setAllPurchaseOrders", res.data)
      commit("updatePurchaseOrderFilter", {startDate: start_date, endDate: end_date, clientName: clientName})
    })
    .catch(error =>
      apiHandler(error)
    )
  },

  claimCredit({ commit }, orderUuid) {
    return securedApi.post(`${pantryApi}/claim_credit/${orderUuid}`)
    .then((res) => {
      commit('setCurrentOrder', res.data.obj)
      return res
    })
    .catch(error => error)
  },

  orderOutstandingItems({ commit }, orderUuid) {
    return securedApi.post(`${pantryApi}/order_outstanding_items/${orderUuid}`)
    .then((res) => {
      commit('setCurrentOrder', res.data.obj)
      return res
    })
    .catch(error => error)
  },

  createPayment({ commit }, {payment, orderUuid}) {
    return securedApi.post(`${pantryApi}/payments/create_payment/${orderUuid}`, payment)
    .then((res) => {
      commit('setCurrentOrder', res.data)
      return res
    })
    .catch(error => error)
  },
}

const isFilteringClient = function (clientFilter) {
  return clientFilter && (clientFilter !== 'All Suppliers' && clientFilter !== 'All Customers') ? true : false
}

const saleStatusAndClientFilter = function (orders, status, saleClientFilter) {
  let order = []
  orders.filter(o =>  {
    if (o.order_status == status && o.customer === saleClientFilter) {
      order.push(o)
    }
  })
  return order
}

const statusAndClientFilter = function (orders, status, clientFilter) {
  let order = []
  orders.filter(o =>  {
    if (o.order_status == status && o.supplier.title === clientFilter) {
      order.push(o)
    }
  })
  return order
}

const statusFilter = function (orders, status) {
  return orders.filter(o =>  o.order_status === status)
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
