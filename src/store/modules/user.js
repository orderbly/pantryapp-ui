import { securedAxiosInstance, plainAxiosInstance } from '../../axios/index' 
import axios from 'axios'
import { pantryApi } from '@/constants/config'
import { apiHandler } from '../../util/apiHandler';
import { getCookie, setCookie, eraseCookie } from '../../constants/cookies'
import router from '../../routes/routes'
let plainApi = plainAxiosInstance
let securedApi = securedAxiosInstance

const isLoggedin = function () {
  return getCookie('access') !== undefined ? true : false
}

const state = {
  permissions: {},
  products: [],
  test: 'Test',
  jwt: null,
  user: null,
  userMemberships: [],
  currentUser: null,
  inviteMessage: null,
  loginError: '',
  isLoading: false,
  filterMembershipBySupplier: true,
  userDetails: {}
}

const getters = {
  userPermissions: state => state.permissions,
  isLoading: state => state.isLoading,
  currentUser() {
    if (getCookie('user')) {
      return JSON.parse(getCookie('user'))
    }
  },
  getUserDetails: state => state.userDetails,
  filterSupplier: state => state.filterMembershipBySupplier,
  getInviteMessage: state => state.inviteMessage,
  getCurrentUser: state => getCookie('user') ? JSON.parse(getCookie('user')) : null,
  getLoginError: state => state.loginError,
  getUserMemberships: state => state.userMemberships,
  getSelectUserMemberships (state) {
    if (state.userMemberships.length) {
      if (state.filterMembershipBySupplier) {
        return state.userMemberships.filter((m) =>m.company !== null && ( m.company.supplier !== null && m.company.supplier == true ))
      } else {
        return state.userMemberships.filter((m) => m.company !== null && (m.company.supplier == null ||  m.company.supplier !== true ))
      }
    }
  },
  isAdmin() {
    if (getCookie('user')) {
      return JSON.parse(getCookie('user')).role == 0
    }
  },
  isCurrentUser() {
    if (getCookie('company') && getCookie('access')) {
      return "complete"
    }
    else if(!getCookie('company') && getCookie('access')) {
      return "login_complete"
    }
    else {
      return "incomplete"
    }
  }
}

const mutations = {

  setPermissions (state) {
    if (getCookie('user') !== null) {
      let userCookie = getCookie('user')
      state.permissions = JSON.parse(atob(JSON.parse(userCookie).permissions))
    }
  },

  signinFailed (state, message) {
    if (message.includes('404')) {
      state.loginError = "Login failed. Email was not found."
    } else if (message.includes('401')) {
      state.loginError = "Login failed. Incorrect Password."
    } else if (message.includes('500')) {
      state.loginError = "An issue has occured. Please try again or contact our team."
    } else {
      state.loginError = "An issue has occured. Please try again or contact our team."
    }
  },

  setMembershipFilter(state, filter) {
    filter == "My Suppliers" ? state.filterMembershipBySupplier = true : state.filterMembershipBySupplier = false
  },

  saveSignInToken(state, token) {
    setCookie('access', token, 10)
    state.jwt = token
    location.replace('/')
  },

  saveRegisterUserToken(state, response) {
    setCookie('access', response.data.access, 10)
    setCookie('refresh', response.data.refresh, 10)
    setCookie('csrf', response.data.csrf, 10)

    state.jwt = response.data
    location.replace('/')
  },

  saveResponseMessage(state, message) {
    state.inviteMessage = message
  },

  setProductError(state, error) {
    state.error = error
  },

  signoutToken(state) {
    state.jwt = null
    location.replace('/')
  },

  setRegisterUser(state, user) {
    state.user = user
    location.replace('/select_comapny')
  },

  setUserMemberships(state, data) {
    state.userMemberships = data
  },

  setLoadingState(state) {
    state.isLoading = !state.isLoading
  },

  saveUserDetail(state) {
    state.userDetails = !state.userDetails
    state.currentUser = !state.currentUser
  },

  updateUserDetails (state, userData) {
    state.current_user = userData
    setCookie('user', JSON.stringify(userData), 10)
  },

  updateProduct(state, product) {
    let products = state.filter(p => p.id !== product.id)
    product.push(product)
    state.products = products
  }
}

const actions = {

  loadingState({ commit }, emailAddress) {
    commit('setLoadingState')
    // .then(location.replace('/'))
  },
  async forgotPasswordEmail({ commit }, emailAddress) {
    const payload = {email: emailAddress}
    return plainApi.post(`/password_resets`, payload)
    .then((response) => {
      return response
    }) 
  },

  reset ({ commit }, details) {
    plainApi.patch(`/password_resets/${details.token}`, details.userDetails)
    .then(response => this.resetSuccessful(response))
    .catch(error => this.resetFailed(error))
  },

  fetchUserMemberships ({ commit, dispatch }) {
    if (!isLoggedin()) {
      return false
    }
    return securedApi.get(`${pantryApi}/user_memberships`, {})
    .then(r => {
      if (r.data.length <= 1 && getCookie('company') == null) {
        dispatch('companies/postCurrentCompany', r.data[0].company.id, {root: true}).then((res) =>{
          return false
        })
      } else {
        commit('setUserMemberships', r.data)
        return true
      }
    })
    .catch(error =>
      apiHandler(error)
    )
  },

  signOut({ commit }) {
    if (!isLoggedin()) {
      return false
    }
    securedApi.post(`/signout/`, {})
    .then(() => {
      console.log('signout')
      eraseCookie('access')
      eraseCookie('refresh')
      eraseCookie('user')
      eraseCookie('csrf')
      eraseCookie('company')
      commit('signoutToken')
    })
    .catch(error => console.log(error))
  },

  invite_user({ commit, dispatch }, inviteData) {
    return securedApi.post(`${pantryApi}/invite_user`,inviteData)
      .then(async function(response) {
        await dispatch('companies/fetchCompanyMemberships', {}, {root: true})
        commit('saveResponseMessage', response.data)
        return response
      })
      .catch((error) =>
        error
      )
  },

  checkPasswordToken({ commit, dispatch }, token) {
    if (!isLoggedin()) {
      return false
    }
    securedApi.get(`/password_resets/${token}`, {})
    .then(response => {
      if (response == undefined) location.replace('/')
    })
    .catch(error =>  {
      location.replace('/')
    })
  },

  async userLogin({ commit, dispatch }, user) {
    let auth = user.auth
    return plainApi.post(`${pantryApi}/signin`, {
      auth
    })
    .then(response => {
      signinSuccessful(response)
    })
    .catch(error => {
      commit('signinFailed', error.message)
    })
      
  },

  googleLogin({ commit, dispatch }, auth) {
    return plainApi.post(`${pantryApi}/get_google_authorization`, {
      id_token: auth
    })
    .then(response => {
      signinSuccessful(response)
      return true
    })
    .catch(error => {
      commit('signinFailed', error.message)
      commit('setLoadingState')
    })
  },

  registerLogin({ commit, dispatch }, user) {
    let auth = user.auth
    return plainApi.post(`${pantryApi}/signin`, {
        auth
      })
      .then(function(response) {
        commit('saveRegisterUserToken', response)
      })
      .catch(error =>
        apiHandler(error)
      )
  },

  getUserDetails({ commit }) {
    securedApi.post(`${pantryApi}/user_details`, {}).then(function(response) {
      commit('saveUserDetail', response.data)
    })
    .catch(error =>
      apiHandler(error)
    )
  },

  registerUser({ commit }, user) {
    commit('setRegisterUser', user)
  },

  updateUser({ commit }, user) {
    return securedApi.patch(`${pantryApi}/users/${user.id}`, {user: user})
    .then(function(response) {
      commit('updateUserDetails', response.data)
      return true
    })
    .catch(() => {
      return false
    })
  },

  removeUserAvatar({ commit }, user) {
    securedApi.post(`${pantryApi}/remove_user_avatar`, {})
    .then(function(response) {
      commit('updateUserDetails', response.data)
    })
  },

  userExists({ commit }, userEmail) {
    return securedApi.post(`${pantryApi}/user_exists`, {email: userEmail})
    .then((response) =>{
      return response
    })
  },

  completeRegistration({ dispatch }, user) {
    plainApi
      .post(`${pantryApi}/signup`, {
        user
      })
      .then(function() {
        dispatch('registerLogin', {
          auth: { email: user.email, password: user.password }
        })
      })
  },

  register({ state, dispatch }, company) {
    let newUser = state.user
    let promise = new Promise(function(resolve, reject) {
      setTimeout(function() {
        resolve(dispatch('completeRegistration', newUser))
      }, 2000)
    })

    promise.then(function(data) {
      setTimeout(function() {
        dispatch('companies/registerCompany', company, { root: true })
      }, 2000)
    })
  }
}

const resetSuccessful = function (response) {
  let notice = 'Your password has been reset successfully! Please sign in with your new password.'
  let error = ''
  let password = ''
  let password_confirmation = ''
  }
  
  const resetFailed = function (error) {
    let err = (error.response && error.response.data && error.response.data.error) || 'Something went wrong'
    let notice = ''
  }
  
  const signinSuccessful =  function (response) {
    if (!response.data.access) {
      this.signinFailed(response)
      return
    }
    
    setCookie('access', response.data.access, 10)
    setCookie('refresh', response.data.refresh, 10)
    setCookie('csrf', response.data.csrf, 10)
    setCookie('usignature', response.data.usignature, 10)
    setCookie('user', response.data.user, 10)
    setCookie('signedIn', true, 10)
    location.replace('/')
  } 


export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
