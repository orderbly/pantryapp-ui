import { securedAxiosInstance, plainAxiosInstance } from '../../axios/index' 
import { pantryApi } from '@/constants/config'

let securedApi = securedAxiosInstance

const actions = {
  sendOrderConfirmation({}, order_uuid) {
    return securedApi.post(`${pantryApi}/emails/send_order_email/${order_uuid}`, {})
    .then(response => { 
        return response
    })
  },

  sendDeliveryOrder({ }, order_uuid) {
    return securedApi.post(`${pantryApi}/emails/send_delivery_email/${order_uuid}`, {})
    .then(response => { 
        return response
    })
  },

  toggleIntegrationModal({ commit }) {
    commit('setIntegrationModal')
  },
  toggleAccountingModal({ commit }) {
    commit('setAccountingModal')
  }

}

export default {
  namespaced: true,
  actions
}
