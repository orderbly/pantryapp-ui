import axios from 'axios'
import { securedAxiosInstance, plainAxiosInstance } from '../../axios/index' 
import { pantryApi } from '@/constants/config'
import { apiHandler } from '../../util/apiHandler';
import { getCookie, setCookie, eraseCookie } from '../../constants/cookies'

let plainApi = plainAxiosInstance
let securedApi = securedAxiosInstance
const isLoggedin = function () {
  return getCookie('access') !== undefined ? true : false
}

const state = {
  suppliers: [],
  items: [],
  companyAddresses: [],
  current_company: null,
  dashboardPurchases: null,
  dashboardSales: null,
  currentSupplier: [],
  activeSupplierBalance: 0,
  currentCredit: 0,
  accountBalance: 0,
  memberships: [],
  filterMemberships: "Suppliers",
  deliveryFees: [],
  customCompanyList: [],
}

const getters = {
  getDeliveryFees: state => state.deliveryFees,
  hasAccountingIntegration: state => state.current_company.toString(),
  dashboardPurchases: state => state.dashboardPurchases,
  dashboardSales: state => state.dashboardSales,
  currentSupplier: state => state.currentSupplier,
  isSupplier: state => { 
    if (state.current_company) {
      return state.current_company.supplier !== false
    } 
    return false
  },
  getAllSuppliers: state => state.suppliers,
  getCompanyAddresses: state => state.companyAddresses.sort((a,b) => a.id - b.id),
  getCurrentCompany: state => state.current_company,
  getCurrentCredit: state => state.currentCredit,
  getCurrentAccontBalane: state => state.accountBalance,
  getCompanyMemberhsips: state => state.memberships,
  getActiveSupplierBalance: state => state.activeSupplierBalance,
  getCustomCompanyList: state => state.customCompanyList
}

const mutations = {
  setDeliveryFees(state, data) {
    state.deliveryFees = data
  },

  setCurrentSupplier(state, data) {
    state.currentSupplier = data
  },

  setSupplierList(state, items) {
    state.suppliers = items
  },

  saveCurrentCompany(state, data) {
    state.current_company = data
  },

  setPurchaseDashboard(state, data) {
    state.dashboardPurchases = data
  },

  setSalesDashboard(state, data) {
    state.dashboardSales = data
  },

  setSupplierListError(state, error) {
    state.supplierErrorState = true
    state.supplierError = error
  },
  setCurrentCredit(state, data) {
    state.currentCredit = data
  },
  setAccountBalance(state, data) {
    state.accountBalance = data
  },
  setMemberships(state, data) {
    state.memberships = data
  },
  setCompanyAddresses(state, data) {
    state.companyAddresses = data
  },
  setNewCompanyAddress(state, data) {
    state.companyAddresses.push(data)
  },
  setEditCompanyAddress(state, data) {
    let addresses = state.companyAddresses.filter(address => address.id !== data.id)
    state.companyAddresses = addresses
    state.companyAddresses.push(data)
    // state.companyAddresses = addresses
  },
  setAllCustomCompanies(state, data) {
    state.customCompanyList = data
  },
  setEditCompanyDetails(state, data) {
    let customCompanies = state.customCompanyList.filter(customCompany => customCompany.id !== data.id)
    state.customCompanyList = customCompanies
    state.customCompanyList.push(data) 
  }

}

const actions = {

  updateCompanyDetails({ commit }, company) {
    if (!isLoggedin()) {
      return false
    }
    return securedApi.patch(`${pantryApi}/companies/${company.id}/`, {company}, {})
    .then((res) => {
      return res
    })
    .catch(error =>
      apiHandler(error)
    )
  },
  postCurrentCompany({ commit }, id) {
    let uniqueKey = Math.random()
    window.name = uniqueKey
    return securedApi.patch(`${pantryApi}/select_current_company`, {company_id: id, unique_key: uniqueKey}, {})
    .then(response => {
      setCookie('company', JSON.stringify(response.data.company), 10)
      setCookie('user', JSON.stringify(response.data.user), 10)
      commit('saveCurrentCompany', response)
      location.replace('/')
      return true
    })
    .then(() => location.replace('/'))
    .catch(error =>
      apiHandler(error)
    )
  },

  fetchCreditBalance({ commit, store }, {supplier_id, customer_id}) {
    securedApi.get(`${pantryApi}/credit_notes/balance/${supplier_id}`, {})
    .then(response => { 
      commit('setCurrentCredit', response.data.balance)
    })
  },

  fetchActiveSupplierBalance({ commit, rootState }) {
    if (rootState.cart.activeSupplier) {
      let activeSupplier = rootState.cart.activeSupplier
      securedApi.get(`${pantryApi}/credit_notes/balance/${activeSupplier}`, {})
      .then(response => { 
        commit('setAccountBalance', response.data.balance)
      })
    }
  },

  fetchAccountBalance({ commit, store }, {supplier_id}) {
    securedApi.get(`${pantryApi}/credit_notes/account/${supplier_id}`, {})
    .then(response => { 
      commit('setAccountBalance', response.data.account)
    })
  },

  async fetchCurrentCompany({ commit, dispatch }, id) {
    if (!isLoggedin()) {
      return false
    }

    return securedApi.get(`${pantryApi}/companies/current_company`, {})
    .then(response => { 
      if (response.data) {
        commit('saveCurrentCompany', response.data)
        return response.data
      }
    }).catch((err) => {
      return err
    })
  },
  fetchCompanyByTitle({ commit, dispatch }, title) {
    dispatch('user/loadingState', null, { root: true })
    if (!isLoggedin()) {
      return false
    }
    securedApi.get(`${pantryApi}/companies/company_by_title/${title}`, {})
    .then(response => { 
      if (response.data) {
        dispatch('user/loadingState', null, { root: true })
        commit('setCurrentSupplier', response.data)
      }
    })
  },
  async registerCompany({ commit, dispatch }, company) {
    return securedApi.post(`${pantryApi}/companies/`, company)
    .then(r => {
      dispatch('user/loadingState', null, { root: true })
      dispatch('postCurrentCompany', r.data.id)
      return true
    })
    .catch(error => {
      apiHandler(error)
      dispatch('user/loadingState', null, { root: true })
      return false
      }
    )
  },
  async registerAddress({ commit, dispatch }, address) {
    return securedApi.post(`${pantryApi}/addresses/`, address)
    .then(r => {
      commit('setNewCompanyAddress', r.data)
      return true
    })
    .catch(error => {
      return false
      }
    )
  },
  async editAddress({ commit, dispatch }, address) {
    return securedApi.patch(`${pantryApi}/addresses/${address.id}`, address)
    .then(r => {
      commit('setEditCompanyAddress', r.data)
      return true
    })
    .catch(error => {
      return false
      }
    )
  },
  async getAddresses({ commit }, address) {
    return securedApi.get(`${pantryApi}/addresses/`)
    .then(r => {
      commit('setCompanyAddresses', r.data)
      return true
    })
    .catch(error => {
      return false
      }
    )
  },
  fetchAllSuppliers({ commit, dispatch }) {
    dispatch('user/loadingState', null, { root: true })
    if (!isLoggedin()) {
      return false
    }
    securedApi.get(`${pantryApi}/companies/`, {})
      .then(r => {
        commit('setSupplierList', r.data)
      })
      .then(res => {
        if (res) {
          dispatch('user/loadingState', null, { root: true })
          res.data
        } else {
          dispatch('user/loadingState', null, { root: true })
          commit('setSupplierListError', 'error:getSurveyItem')
        }
      })
      .catch(error =>
        apiHandler(error)
      )
  },
  dashboardPurchases({ commit, dispatch }) {
    if (!isLoggedin()) {
      return false
    }
    securedApi.get(`${pantryApi}/companies/purchases_dashboard/`, {})
      .then(r => {
        commit('setPurchaseDashboard', r.data)
      })
      .catch(error =>
        apiHandler(error)
      )
  },

  dashboardSales({ commit }) {
    if (!isLoggedin()) {
      return false
    }
    securedApi.get(`${pantryApi}/companies/sales_dashboard/`, {})
      .then(r => {
        commit('setSalesDashboard', r.data)
      })
      .catch(error =>
        apiHandler(error)
      )
  },

  deleteMembership({commit}, member_id) {
    if (!isLoggedin()) {
      return false
    }
    securedApi.post(`${pantryApi}/companies/delete_memberships`, {member_id})
      .then(r => {
        commit('setMemberships', r.data)
      })
      .catch(error =>
        apiHandler(error)
      )
  },

  deleteInvite({commit}, invite_id) {
    return securedApi.post(`${pantryApi}/companies/delete_invite`, {invite_id})
      .then(response => {
        return response
      })
  },

  updateDefaultAddress({commit}, {company_id, address_id}) {
    return securedApi.post(`${pantryApi}/companies/update_default_address/${company_id}/${address_id}`)
      .then(res => {
        setCookie('company', JSON.stringify(res.data), 10)
        commit('saveCurrentCompany', res.data)
        return res
      })
      .catch(error =>
        apiHandler(error)
      )
  },

  updateDefaultCustomAddress({commit}, {company_id, address_id}) {
    securedApi.post(`${pantryApi}/companies/update_default_address/${company_id}/${address_id}`)
      .then(res => {
        return res
      })
      .catch(error =>
        apiHandler(error)
      )
  },

  postNewCompanyEmail({commit}, email_addresses) {
    return securedApi.post(`${pantryApi}/companies/add_emails`, email_addresses)
      .then(res => {
        setCookie('company', JSON.stringify(res.data.obj), 10)
        commit('saveCurrentCompany', res.data.obj)
        return res
      })
      .catch(error => error)
  },

  deleteCompanyEmail({commit}, email_addresses) {
    return securedApi.post(`${pantryApi}/companies/delete_email`, email_addresses)
      .then(res => {
        setCookie('company', JSON.stringify(res.data.obj), 10)
        commit('saveCurrentCompany', res.data.obj)
        return res
      })
      .catch(error => error)
  },

  fetchCompanyMemberships({ commit }) {
    if (!isLoggedin()) {
      return false
    }
    return securedApi.get(`${pantryApi}/companies/company_memberships`, {})
    .then(r => {
      commit('setMemberships', r.data)
      return r.data
    })
    .catch(error => {
      apiHandler(error)
      return false
    })
  },

  fetchMembership({ commit }, {companyId, uuid}) {
    return securedApi.get(`${pantryApi}/members/get_member/${uuid}/${companyId}`, {})
    .then(r => {
      return r.data
    })
    .catch(error => {
      apiHandler(error)
      return false
    })
  },

  updateMembership({ commit }, data) {
    return securedApi.patch(`${pantryApi}/members/update_member/`, data)
    .then(response => {
      return response.data
    })
    .catch(error => {
      return error
    })
  },

  fetchPendingInvites({ commit }, companyUuid) {
    return securedApi.get(`${pantryApi}/companies/company_invites/${companyUuid}`)
    .then(response => {
      return response
    })
    .catch(error => {
      apiHandler(error)
      return false
    })
  },

  fetchSlimCompanyList({ commit }, companyId) {
    return securedApi.get(`${pantryApi}/companies/slim_company_list/${companyId}`, {})
    .then(r => r.data)
    .catch(error => error)
  },

  fetchDeliveryFees({ commit }, companyUuid) {
    return securedApi.get(`${pantryApi}/delivery_fee_brackets/company_delivery_brackets/${companyUuid}`, {})
    .then(response => {
      commit('setDeliveryFees', response.data)
      return response.data
    })
    .catch(error => error)
  },

  deleteDeliveryFee({ commit }, distance_fee_id) {
    return securedApi.delete(`${pantryApi}/delivery_fee_brackets/${distance_fee_id}`, {})
    .then(response => {
      commit('setDeliveryFees', response.data)
      return response.data
    })
    .catch(error => error)
  },

  newDeliveryBracket({ commit }, newDeliveryBracket) {
    return securedApi.post(`${pantryApi}/delivery_fee_brackets`, newDeliveryBracket)
    .then(response => {
      commit('setDeliveryFees', response.data)
      return response
    })
    .catch(error => error)
  },

  updateCompanyLogo({ commit }, {image, companyUuid}) {
    return securedApi.post(`${pantryApi}/companies/update_company_logo/${companyUuid}`, {image})
    .then(response => {
      setCookie('company', JSON.stringify(response.data.company), 10)
      commit('saveCurrentCompany', response)
      return response
    })
    .catch(error => error)
  },

  getDefaultDeliveryReconciliation({ commit }, companyUuid) {
    return securedApi.get(`${pantryApi}/companies/delivery_reconciliation/${companyUuid}`)
    .then(response => {
      return response
    })
    .catch(error => error)
  },

  updateDefaultDeliveryReconciliation({ commit }, {company_uuid, default_delivery_reconciliation}) {
    return securedApi.post(`${pantryApi}/companies/update_delivery_reconciliation/${company_uuid}`, {default_delivery_reconciliation})
    .then(response => {
      return response
    })
    .catch(error => error)
  },
  
  updateDefaultDeliverySignature({ commit }, {company_uuid, is_delivery_signature_required}) {
    return securedApi.post(`${pantryApi}/companies/update_delivery_signature/${company_uuid}`, {is_delivery_signature_required})
    .then(response => {
      return response
    })
    .catch(error => error)
  },

  getDefaultDeliverySignature({ commit }, companyUuid) {
    return securedApi.get(`${pantryApi}/companies/delivery_signature/${companyUuid}`)
    .then(response => {
      return response
    })
    .catch(error => error)
  },

  updateCompanyBanner({ commit }, {image, companyUuid}) {
    return securedApi.post(`${pantryApi}/companies/update_company_banner/${companyUuid}`, {image})
    .then(response => {
      setCookie('company', JSON.stringify(response.data.company), 10)
      commit('saveCurrentCompany', response)
      return response
    })
    .catch(error => error)
  },

  findCompanyByTitle({ commit }, {query, company_uuid}) {
    return securedApi.get(`${pantryApi}/companies/find_company_by_title/${company_uuid}/${query}`,)
    .then(response => {
      return response
    })
    .catch(error => error)
  },

  findAllCustomCompanies({ commit }, company_uuid) {
    return securedApi.get(`${pantryApi}/companies/find_all_custom_companies/${company_uuid}`,)
    .then(response => {
      commit('setAllCustomCompanies', response.data)
      return response
    })
    .catch(error => error)
  },

  allCustomers({ commit }, company_uuid) {
    return securedApi.get(`${pantryApi}/companies/all_customers/${company_uuid}`,)
    .then(response => {
      commit('setAllCustomCompanies', response.data)
      return response
    })
    .catch(error => error)
  },

  createCustomClient({ commit }, {custom_client, supplier_id}) {
    return securedApi.post(`${pantryApi}/companies/create_custom_customer/${supplier_id}`, custom_client)
    .then(response => {
      return response
    })
    .catch(error => error)
  },

  async editCustomClient({ commit }, {custom_client}) {
    return securedApi.patch(`${pantryApi}/companies/${custom_client.id}`, custom_client)
    .then(response => {
      commit('setEditCompanyDetails', response.data)
      return response
    })
  },

  fetchAllAreas({commit}, query) {
    return securedApi.get(`${pantryApi}/companies/all_areas/${query}`)
    .then(response => {
      return response
    })
    .catch(error => error)
  },

  fetchAllProvinces({commit}, query) {
    return securedApi.get(`${pantryApi}/companies/all_provinces`)
    .then(response => {
      return response
    })
    .catch(error => error)
  },

  fetchAllCities({commit}, province) {
    return securedApi.get(`${pantryApi}/companies/all_cities/${province}`)
    .then(response => {
      return response
    })
    .catch(error => error)
  },

  fetchAllAreasByProvince({commit}, {province}) {
    return securedApi.get(`${pantryApi}/companies/area_by_province/${province}`)
    .then(response => {
      return response
    })
    .catch(error => error)
  },

  fetchMyCustomer({commit}, {companyUuid, customerUuid}) {
    return securedApi.get(`${pantryApi}/companies/fetch_my_customer/${companyUuid}/${customerUuid}`)
    .then(response => {
      return response
    })
    .catch(error => error)
  },

  fetchAllSuburbs({commit}, {city}) {
    return securedApi.get(`${pantryApi}/companies/all_suburbs/${city}`)
    .then(response => {
      return response
    })
    .catch(error => error)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
