const state = {
  // product
  displayProductModal: false,
  displaySelectProductModal: false,
  
  // Tags
  displayNewTagModal: false,

  // Users
  displayInviteUserModal: false,

  // Custom Client / Orders
  displayCustomClientSelect: false,

  // Deliveries

  displayNewCustomClientModal: false,
  displayCustomDelivery: false,
  displayUpdateDeliveryDetails: false,
  
  // Company
  displayNewCompanyModal: false,
  displaySignupModal: false,
  emailForgotPasswordModal: false,
  disaplyNewAddressModal: false,
  disaplyEditAddressModal: false,
  displayNewEmailModal: false,
  displayNewDeliveryFee: false,

  //Supplier
  displaySupplierDetailsModal: false,

  // Orders
  directPaymentModal: false,
  displayDeliveryDate: false,
  displayAllocateDriver: false,
  displayRecurringModal: false,
  displayOrderDeliveryOptions: false,
  signModal: false,
  displayConfirmDelivery: false,
  displayDeliveryReconciliation: false,
  displayPaymentReconciliation: false,
  displayCreditNoteModal: false,
  displayGeneralCreditNoteModal: false,
  displayOrderProductSelectModal: false,
  displayEditDeliveryFee: false,
  displayTradegeckoCustomerAllocation: false,

  // price book
  displayPriceBookMembershipModal: false,
  displayNewPriceBookModal: false,

  // Products

  displayTradegeckoProductModal: false,

   // Credit Limit
  displayNewCreditLimit: false,
  displayEditCreditLimit: false,
  
  // Tax
  displayNewTaxModal: false,
  displayEditTaxModal: false,

  // Cart
  displayPaymentOptionModal: false,
  displayMobileCart: false,

  items: [],
  payAccountModal: false
}

const getters = {

  // Product
  getDisplayProductModal: state => state.displayProductModal,
  displaySelectProductModal: state => state.displaySelectProductModal,

  // User
  getDisplayInviteUserModal: state => state.displayInviteUserModal,

  // deliveries
  getDisplayNewCustomClientModal: state => state.displayNewCustomClientModal,
  getCustomDelivery: state => state.displayCustomDelivery,
  getDisplayUpdateDeliveryDetails: state => state.displayUpdateDeliveryDetails,

  //Supplier
  displaySupplierDetailsModal: state => state.displaySupplierDetailsModal,

  // Order
  getDirectPaymentModal: state => state.directPaymentModal,
  getDisplayEditDeliveryDate: state => state.displayDeliveryDate,
  getDisplayGeneralCreditNoteModal: state => state.displayGeneralCreditNoteModal,
  getDisplayCreditNoteModal: state =>  state.displayCreditNoteModal,
  getDisplayRecurringModal: state => state.displayRecurringModal,
  getAllocateDriverModal: state => state.displayAllocateDriver,
  // getDisplayConfirmDelivery: state => state.displayConfirmDelivery,
  getDisplayInvoiceOptions: state => state.displayOrderDeliveryOptions,
  getDisplayDeliveryReconciliation: state => state.displayDeliveryReconciliation,
  getDisplayPaymentReconciliation: state => state.displayPaymentReconciliation,
  getDisplayConfirmDelivery: state => state.displayConfirmDelivery,
  getSignModal: state => state.signModal,
  getOrderProductSelectModal: state => state.displayOrderProductSelectModal,
  getDisplayEditDeliveryFee: state => state.displayEditDeliveryFee,
  getTradegeckoCustomerAllocation: state => state.displayTradegeckoCustomerAllocation,

  // Price Book
  getNewPriceBookModal: state => state.displayNewPriceBookModal,
  getPriceBookMembershipModal: state => state.displayPriceBookMembershipModal,

  // Products
  getTradegeckoProductModal: state => state.displayTradegeckoProductModal,

  // Custom Client / Order
  getDisplayCustomClientSelect: state => state.displayCustomClientSelect,

  // Credit Limit
  getDisplayNewCreditLimit: state => state.displayNewCreditLimit,
  getDisplayEditCreditLimit: state => state.displayEditCreditLimit,

  // Tag
  getNewTagModal: state => state.displayNewTagModal,

  // Cart
  getPaymentOptionModal: state => state.displayPaymentOptionModal,
  getDisplayMobileCart: state => state.displayMobileCart,

  // Tax
  getNewTaxModal: state => state.displayNewTaxModal,
  getEditTaxModal: state => state.displayEditTaxModal,

  // Company
  getNewCompanyModal: state => state.displayNewCompanyModal,
  getSignUpModal: state => state.displaySignupModal,
  getEmailForgotPasswordModal: state => state.emailForgotPasswordModal,
  getNewAddressModal: state => state.disaplyNewAddressModal,
  getEditAddressModal: state => state.disaplyEditAddressModal,
  getNewEmailModal: state => state.displayNewEmailModal,
  getNewDeliveryFee: state => state.displayNewDeliveryFee,
  
  // Statements
  getPayAccountModal: state => state.payAccountModal
  
}

const mutations = {

  toggleDisplayCustomClientSelect() {
    state.displayCustomClientSelect = !state.displayCustomClientSelect
  },

  toggleDisplayTradegeckoProductModal() {
    state.displayTradegeckoProductModal = !state.displayTradegeckoProductModal
  },

  toggleDisplayUpdateDeliveryDetails() {
    state.displayUpdateDeliveryDetails = !state.displayUpdateDeliveryDetails
  },

  toggleDisplayTradegeckoCustomerAllocation() {
    state.displayTradegeckoCustomerAllocation = !state.displayTradegeckoCustomerAllocation
  },
  
  toggleDisplayEditDeliveryFee() {
    state.displayEditDeliveryFee = !state.displayEditDeliveryFee
  },
  toggleNewDeliveryFee() {
    state.displayNewDeliveryFee = !state.displayNewDeliveryFee
  },

  toggleDisplayNewCustomClientModal () {
    state.displayNewCustomClientModal = !state.displayNewCustomClientModal
  },
  toggleDisplayCustomDelivery () {
    state.displayCustomDelivery = !state.displayCustomDelivery
  },

  toggleDirectPaymentModal () {
    state.directPaymentModal = !state.directPaymentModal
  },

  toggleDisplayEditDeliveryDate () {
    state.displayDeliveryDate = !state.displayDeliveryDate
  },

  toggleEditCreditLimit () {
    state.displayEditCreditLimit = !state.displayEditCreditLimit
  },

  toggleDisplayNewCreditLimit () {
    state.displayNewCreditLimit = !state.displayNewCreditLimit
  },

  toggleDisplayNewEmailModal () {
    state.displayNewEmailModal = !state.displayNewEmailModal;
  },

  toggleOrderProductSelectModal () {
    state.displayOrderProductSelectModal = !state.displayOrderProductSelectModal;
  },

  toggleEditAddressModal () {
    state.disaplyEditAddressModal = !state.disaplyEditAddressModal;
  },

  toggleDisplayGeneralCreditNoteModal() {
    state.displayGeneralCreditNoteModal = !state.displayGeneralCreditNoteModal;
  },

  toggleNewAddressModal () {
    state.disaplyNewAddressModal = !state.disaplyNewAddressModal;
  },

  toggleCreditNote () {
    state.displayCreditNoteModal = !state.displayCreditNoteModal;
  },

  togglePaymentReconciliation () {
    state.displayPaymentReconciliation = !state.displayPaymentReconciliation;
  },

  toggleDeliveryReconciliation () {
    state.displayDeliveryReconciliation = !state.displayDeliveryReconciliation;
  },

  setOrderDeliveryOptions (state) {
    state.displayOrderDeliveryOptions = !state.displayOrderDeliveryOptions;
  },

  toggleSignModal(state) {
    state.signModal = !state.signModal;
  },

  setDisplayConfirmDelivery (state) {
    state.displayConfirmDelivery = !state.displayConfirmDelivery;
  },

  setDisplayRecurringModal (state) {
    state.displayRecurringModal = !state.displayRecurringModal
  },

  setPayAccountModal (state) {
    state.payAccountModal = !state.payAccountModal
  },

  setAllocateDriver(state) {
    state.displayAllocateDriver = !state.displayAllocateDriver
  },

  setPaymentOptionModal(state) {
    state.displayPaymentOptionModal = !state.displayPaymentOptionModal
  },

  setSignupModal(state) {
    state.displaySignupModal = !state.displaySignupModal
  },

  setDisplayProductModal(state) {
    state.displayProductModal = !state.displayProductModal
  },

  setDisplayInviteUserModal(state) {
    state.displayInviteUserModal = !state.displayInviteUserModal
  },

  setSelectProductModal(state) {
    state.displaySelectProductModal = !state.displaySelectProductModal
  },

  setSupplierDetailsModal(state) {
    state.displaySupplierDetailsModal = !state.displaySupplierDetailsModal
  },

  setNewTagModal(state) {
    state.displayNewTagModal = !state.displayNewTagModal
  },

  setNewTaxModal(state) {
    state.displayNewTaxModal = !state.displayNewTaxModal
  },

  setEditTaxModal(state) {
    state.displayEditTaxModal = !state.displayEditTaxModal
  },

  setEmailForgotPasswordModal(state) {
    state.emailForgotPasswordModal = !state.emailForgotPasswordModal
  },

  setNewCompanyModal(state) {
    state.displayNewCompanyModal = !state.displayNewCompanyModal
  },

  setNewPriceBookModal(state) {
    state.displayNewPriceBookModal = !state.displayNewPriceBookModal
  },

  setPriceBookMembershipModal (state) {
    state.displayPriceBookMembershipModal = !state.displayPriceBookMembershipModal
  },
  toggleDisplayMobileCart (state) {
    state.displayMobileCart = !state.displayMobileCart
  }
}

const actions = {
  togglePayAccountModal({ commit }) {
    commit('setPayAccountModal')
  },
  togglePaymentOptionModal({ commit }) {
    commit('setPaymentOptionModal')
  },
  toggleEmailForgotPasswordModal({ commit }) {
    commit('setEmailForgotPasswordModal')
  },
  toggleSignupModal({ commit }) {
    commit('setSignupModal')
  },
  toggleNewTaxModal({ commit }) {
    commit('setNewTaxModal')
  },
  toggleEditTaxModal({ commit }) {
    commit('setEditTaxModal')
  },
  toggleProductModal({ commit }) {
    state.displayProductModal = !state.displayProductModal
  },
  toggleInviteUserModal({ commit }) {
    commit('setDisplayInviteUserModal')
  },
  toggleSelectProductModal({ commit }) {
    commit('setSelectProductModal')
  },
  toggleSupplierDetailsModal({ commit }) {
    commit('setSupplierDetailsModal')
  },
  toggleNewCompanyModal({ commit }) {
    commit('setNewCompanyModal')
  },
  toggleNewTagModal({ commit }) {
    commit('setNewTagModal')
  },
  toggleNewPriceBookModal({ commit }) {
    commit('setNewPriceBookModal')
  },
  togglePriceBookMembershipModal({ commit }) {
    commit('setPriceBookMembershipModal')
  },
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
