import axios from 'axios'
import { securedAxiosInstance } from '../../axios/index' 
import { pantryApi } from '@/constants/config'

let securedApi = securedAxiosInstance

const state = {
  deliveryZones: [],
  deliveryZone: []
}

const getters = {
  deliveryZones: state => state.deliveryZones,
  deliveryZone: state => state.deliveryZone
}

const mutations = {
  saveDeliveryZones(state, data) {
    state.deliveryZones = data
  },
  saveDeliveryZone(state, data) {
    state.deliveryZone = data
  },
}

const actions = {

  getDeliveryZones({ commit }, uuid) {
    return securedApi.get(`${pantryApi}/delivery_zones/company_delivery_zones/${uuid}`)
    .then(response => {
      commit('saveDeliveryZones', response.data)
      return response
    })
    .catch(() => {
      return false
    })
  },

  newDeliveryZone({ commit }, {deliveryZone}) {
    return securedApi.post(`${pantryApi}/delivery_zones`, {delivery_zone: deliveryZone})
    .then(response => {
      commit('saveDeliveryZones', response.data)
      return response
    })
    .catch(() => {
      return false
    })
  },

  updateDeliveryZone({ commit }, {deliveryZone}) {
    return securedApi.patch(`${pantryApi}/delivery_zones/${deliveryZone.id}`, {delivery_zone: deliveryZone})
    .then(response => {
      commit('saveDeliveryZone', response.data)
      return response
    })
    .catch(() => {
      return false
    })
  },

  getDeliveryZone({ commit }, uuid) {
    return securedApi.get(`${pantryApi}/delivery_zones/single_delivery_zone/${uuid}`)
    .then(response => {
      commit('saveDeliveryZone', response.data)
      return response
    })
    .catch(() => {
      return false
    })
  },

  addDeliveryAreas({ commit }, {areas, uuid}) {
    return securedApi.post(`${pantryApi}/delivery_zones/add_areas_to_delivery_zone/${uuid}`, {delivery_areas: areas})
    .then(response => {
      commit('saveDeliveryZone', response.data)
      return response
    })
    .catch(() => {
      return false
    })
  },

  newDeliveryZoneFee({ commit }, {delivery_zone_fee, zoneUuid}) {
    return securedApi.post(`${pantryApi}/delivery_zones/add_delivery_zone_fees/${zoneUuid}`, delivery_zone_fee)
    .then(response => {
      commit('saveDeliveryZone', response.data)
      return response
    })
    .catch(() => {
      return false
    })
  },

  updateDeliveryZoneFee({ commit }, feeBracket) {
    return securedApi.patch(`${pantryApi}/delivery_zones/update_delivery_zone_fees/${feeBracket.id}`, feeBracket)
    .then(response => {
      commit('saveDeliveryZone', response.data.obj)
      return response
    })
    .catch(() => {
      return false
    })
  },

  getSupplierDeliveryDays({ dispatch }, {addressId, value, supplierId}) {
    return securedApi.get(`${pantryApi}/delivery_zones/supplier_delivery_days/${addressId}/${supplierId}`)
    .then(response => {
      return response
    })
    .catch(() => {
      return false
    })
  },

  fetAllAvailableSuburbs({commit}, {companyUuid, city}) {
    return securedApi.get(`${pantryApi}/delivery_zones/all_available_suburbs/${companyUuid}/${city}`)
    .then(response => {
      return response
    })
    .catch(error => error)
  },
  
  allAvailableAreas({commit}, {companyUuid, query}) {
    return securedApi.get(`${pantryApi}/delivery_zones/all_available_areas/${companyUuid}/${query}`)
    .then(response => {
      return response
    })
    .catch(error => error)
  },

  getDeliveryFee({commit}, {addressId, quantity, value, supplierId}) {
    return securedApi.get(`${pantryApi}/delivery_zones/delivery_fee/${addressId}/${quantity}/${value}/${supplierId}`)
    .then(response => {
      commit('cart/setDeliveryFee', response.data.delivery_fee, {root: true})
      return response
    })
    .catch(error => error)
  },

  deleteDeliveryZoneFee({commit}, deliveryZoneFeeId) {
    return securedApi.patch(`${pantryApi}/delivery_zones/delete_delivery_zone_fee/${deliveryZoneFeeId}`)
    .then(response => {
      commit('saveDeliveryZone', response.data)
      return response
    })
    .catch(error => error)
  },

  deleteDeliveryZoneArea({commit}, deliveryZoneFeeId) {
    return securedApi.patch(`${pantryApi}/delivery_zones/delete_delivery_zone_area/${deliveryZoneFeeId}`)
    .then(response => {
      commit('saveDeliveryZone', response.data)
      return response
    })
    .catch(error => error)
  },

  deleteDeliveryZone({commit}, deliveryZoneId) {
    return securedApi.patch(`${pantryApi}/delivery_zones/delete_delivery_zone/${deliveryZoneId}`)
    .then(response => {
      commit('saveDeliveryZone', response.data)
      return response
    })
    .catch(error => error)
  },

  updateDeliveryOrder({ commit }, {companyUuid, list, userId}) {
    return securedApi.post(`${pantryApi}/update_delivery_order/${companyUuid}/${userId}`, {orders: list})
    .then(res => {
      if (res) {
        commit('fulfilmentOrders/setMyDeliveryOrders', res.data, {root: true})
        return true
      }
    })
    .catch((err) => {
      return false
    })
  },

}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
