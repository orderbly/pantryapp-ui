import Vue from 'vue'

Vue.filter('capitalize', function(value) {
  value = value
    .toString()
    .replace(/([A-Z])/g, ' $1')
    .replace(/_/g, ' ')
    .replace(/^./, function(str) {
      return str.toUpperCase()
    })
  return value
})

Vue.filter('titleCase', (str) => {
  return str.replace(
    /\w\S*/g,
    function(txt) {
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    }
  );
})

Vue.filter('currency', function(n) {
  if (n !== 0 && n !== undefined && n !== 'null' && n !== null) {
    return 'R' + n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')
  } else {
    return 'R0.00'
  }
})

Vue.filter('dateSuffix', function(i) {
  var j = i % 10,
      k = i % 100;
  if (j == 1 && k != 11) {
      return i + "st";
  }
  if (j == 2 && k != 12) {
      return i + "nd";
  }
  if (j == 3 && k != 13) {
      return i + "rd";
  }
  return i + "th";
})

Vue.filter('round', function(n) {
  Math.round(n * 100) / 100
})

Vue.filter('datePickerFormat', function(date) {
  var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

  if (month.length < 2) 
      month = '0' + month;
  if (day.length < 2) 
      day = '0' + day;

  return [year, month, day].join('-');
})
Vue.filter('rowTextColour', function(status) {
  switch (status) {
    case "completed":
      return "#5e62e4";
    case "accepted":
      return "#00c781";
    case "pending":
      return "#5603ad";
    case "declined":
      return "#f5365c";
    case "cancelled":
      return "#8898aa";
    default:
      return "#8898aa";
  }
})

Vue.filter('rowColour', function(status) {
  switch (status) {
    case "completed":
      return "10px solid #5e62e4";
    case "cancelled":
      return "10px solid #8898aa";
    case "accepted":
      return "10px solid #00c781";
    case "pending":
      return "10px solid #5603ad";
    case "declined":
      return "10px solid #f5365c";
    default:
      return "10px solid #00c781";
  }
})

Vue.filter('orderStatusBadge', function(status) {
  switch (status) {
    case "completed":
      return "badge-primary";
    case "accepted":
      return "badge-success";
    case "pending":
      return "badge-indigo";
    case "declined":
      return "badge-danger";
    case "cancelled":
      return "badge-darker";
    default:
      return "badge-warning";
  }
})

Vue.filter('getInitials', function(first_name, last_name) {
  return `${first_name.charAt(0)}${last_name.charAt(0)}`
})

// Allows Decimal Numbers with only one "."
Vue.filter('isNumber', function(evt) {
  if (evt.target.value.includes(".") && evt.key === ".") {
    evt.preventDefault();    
  }
  if (evt.key == "Backspace" || evt.key == "ArrowRight" || evt.key == "ArrowLeft") return true
    evt = (evt) ? evt : window.event;
    let charCode = (evt.which) ? evt.which : evt.keyCode;
    if ((charCode > 31 && (charCode < 48 || charCode > 57)) && charCode !== 46 && evt.key !== ".") {
      evt.preventDefault();
    } else {
      return true;
  }
  }
)

// Allows whole numbers with no "."
Vue.filter('isWholeNumber', function(evt) {
  if (evt.key == "Backspace" || evt.key == "ArrowRight" || evt.key == "ArrowLeft") return true
    evt = (evt) ? evt : window.event;
    let charCode = (evt.which) ? evt.which : evt.keyCode;
    if ((charCode > 31 && (charCode < 48 || charCode > 57))) {
      evt.preventDefault();
    } else {
      return true;
    }
  }
)

Vue.filter('isBetween', function(min, max, evt) {
  if (evt.key == "Backspace" || evt.key == "ArrowRight" || evt.key == "ArrowLeft") return true
  if (parseInt(evt.key) !== NaN) {
    let inputVal = evt.target.value.toString() + evt.key
    let val = parseInt(inputVal)
    if (val == null || val == undefined || val > max || val < min) {
      evt.preventDefault();
    } else {
      return true
    }
  }
})

Vue.filter('emailValidation', function(email) {
  let reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,24}))$/
    if(email == null || email == '') {
      return "Please Enter Email";
    } else if(!reg.test(email)) {
      return "Please Enter Correct Email";
    } else {
      return ""
    }
})