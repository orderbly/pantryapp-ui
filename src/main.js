import Vue from 'vue'
import { store } from './store'
import { pusherKey, isProduction } from './constants/config'
import DashboardPlugin from './plugins/dashboard-plugin'
import App from './App.vue'
import '@/filters/index.js'
import VueMq from 'vue-mq'
import PulseLoader from 'vue-spinner/src/PulseLoader.vue'
import Vuelidate from 'vuelidate'
import GAuth from 'vue-google-oauth2'
import * as VueGoogleMaps from 'vue2-google-maps'
import VueSkeletonLoading from 'vue-skeleton-loading';
import VueSignaturePad from "vue-signature-pad";
import JsonExcel from 'vue-json-excel'
import VueClipboard from 'vue-clipboard2'
import VueCryptojs from 'vue-cryptojs'

Vue.use(VueClipboard)
import { ElementTiptapPlugin } from 'element-tiptap';
import VueLoading from 'vuejs-loading-plugin'
import Print from 'vue-print-nb'
import vueDebounce from 'vue-debounce'
import Vuesax from 'vuesax'

import 'element-ui/lib/theme-chalk/index.css';
import 'element-tiptap/lib/index.css';
import 'vuesax/dist/vuesax.css';
Vue.use(Vuesax)
Vue.use(VueCryptojs)
Vue.use(Print);
Vue.use(vueDebounce)
Vue.use(ElementTiptapPlugin)
Vue.component('downloadExcel', JsonExcel)
Vue.use(VueSignaturePad);
Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyCrV4J1_AL5YtrlFtanlHGMRt1dhbBwWew", libraries: 'visualization,places'
  }
})
const gauthOption = {
  clientId: '1094040718781-ddgetiblvb4k7i4387d8j38r5v7gkbtu.apps.googleusercontent.com',
  scope: 'profile email',
  redirect_uri: 'https://6cfb37b7.ngrok.io',
  prompt: 'select_account'
}
Vue.use(GAuth, gauthOption)
Vue.use(Vuelidate)
Vue.use(VueSkeletonLoading);
Vue.use(VueLoading, {
  dark: true, // default false
  text: 'This may take a minute ...', // default 'Loading'
  loading: false, // default false
  background: 'rgb(255,255,255)', // set custom background
  classes: ['myclass'] // array, object or string
})


if(isProduction) {
  if ("serviceWorker" in navigator) {
    if (navigator.serviceWorker.controller) {
      console.log("[PWA Builder] active service worker found, no need to register");
    } else {
      // Register the service worker
      navigator.serviceWorker.register("sw.js", {scope: "./"}).then(function (reg) {
        console.log("[PWA Builder] Service worker has been registered for scope: " + reg.scope);
      });
    }
  }
}

Vue.use(require('vue-pusher'), {
    api_key: pusherKey,
    options: {
        cluster: 'eu',
        encrypted: true,
    }
});

// router setup
import router from './routes/routes'
// plugin setup
Vue.use(DashboardPlugin)

Vue.use(VueMq, {
  breakpoints: {
    mobile: 577,
    tablet: 769,
    desktop: Infinity
  },
  defaultBreakpoint: 'desktop'
})

Vue.prototype.$eventHub = new Vue();

/* eslint-disable no-new */
new Vue({
  comments: {PulseLoader},
  el: '#app',
  store,
  render: h => h(App),
  router
})
